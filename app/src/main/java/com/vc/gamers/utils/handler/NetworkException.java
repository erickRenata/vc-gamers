package com.vc.gamers.utils.handler;

import com.vc.gamers.base.BaseResponse;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class NetworkException extends Exception {

    private final int mResponseCode;
    private final BaseResponse mBaseResponse;

    public NetworkException(BaseResponse baseResponse, int responseCode) {
        this.mBaseResponse = baseResponse;
        this.mResponseCode = responseCode;
    }

    public BaseResponse getResponse() {
        return mBaseResponse;
    }

    public int getResponseCode() {
        return mResponseCode;
    }

}

