package com.vc.gamers.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import com.vc.gamers.VCGamersApp;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class Utils {

    public static int convertToPx(int dp) {
        // Get the screen's density scale
        final float scale = VCGamersApp.getContext().getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (dp * scale + 0.5f);
    }

    public static String formatDate(String dateString) {
        String result = "";
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = fmt.parse(dateString);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM yyyy");
            result = fmtOut.format(date);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String formatDateMappingProfile(String dateString) {
        String result = "";
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = fmt.parse(dateString);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd - MM - yyyy");
            result = fmtOut.format(date);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String formatDateForUpdateParam(String dateString) {
        String result = "";
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("dd - MM - yyyy");
            Date date = fmt.parse(dateString);
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
            result = fmtOut.format(date);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getSpecificTime(String dateString, String type) {
        String result = "";
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = fmt.parse(dateString);
            SimpleDateFormat fmtOut;
            if (type.equalsIgnoreCase("year")) {
                fmtOut = new SimpleDateFormat("yyyy");
            } else if (type.equalsIgnoreCase("month")) {
                fmtOut = new SimpleDateFormat("MM");
            } else {
                fmtOut = new SimpleDateFormat("dd");
            }
            result = fmtOut.format(date);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    public static String getCurrentDate(boolean isExpired) {
        Date currentTime = new Date();
        if (!isExpired) {
            currentTime = Calendar.getInstance().getTime();
        } else {
            Calendar c = Calendar.getInstance();
            c.setTime(currentTime);
            c.add(Calendar.DATE, 1);
            currentTime = c.getTime();
        }
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd / MM / yyyy");
        return fmtOut.format(currentTime);
    }

    public static boolean isValidEmail(EditText editText) {
        String email = editText.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        return email.matches(emailPattern);
    }

    public static boolean isValidPhoneNumber(EditText editText) {
        String phoneNumber = editText.getText().toString().trim();
        if (phoneNumber.length() < 9) {
            return false;
        } else if (phoneNumber.length() > 13) {
            return false;
        } else return Integer.parseInt(phoneNumber.substring(0, 1)) == 8;
    }

    public static String convertNumberWithoutCurrency(int number) {
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        String result;
        String recentLanguage = Locale.getDefault().getDisplayLanguage();
        if (recentLanguage.equals("Bahasa Indonesia")) {
            result = kursIndonesia.format(number);
        } else if (recentLanguage.equals("Indonesia")) {
            result = kursIndonesia.format(number);
        } else {
            result = kursIndonesia.format(number).substring(0, kursIndonesia.format(number).length() - 3);
        }
        return result;
    }

    public static String convertNumberWithRp(int number) {
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("IDR ");
        formatRp.setMonetaryDecimalSeparator('.');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        String result;
        String recentLanguage = Locale.getDefault().getDisplayLanguage();
        if (recentLanguage.equals("Bahasa Indonesia")) {
            result = kursIndonesia.format(number);
        } else if (recentLanguage.equals("Indonesia")) {
            result = kursIndonesia.format(number);
        } else {
            result = kursIndonesia.format(number).substring(0, kursIndonesia.format(number).length() - 3);
        }
        return result;
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static String toTitleCase(String str) {
        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }
}
