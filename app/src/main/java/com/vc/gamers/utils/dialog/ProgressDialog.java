package com.vc.gamers.utils.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseDialogFragment;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class ProgressDialog extends BaseDialogFragment {
    private static final String MESSAGE = "message";

    public static ProgressDialog create() {
        return create("");
    }

    public static ProgressDialog create(String message) {
        Bundle bundle = new Bundle();
        bundle.putString(MESSAGE, message);
        ProgressDialog progressDialogFragment = new ProgressDialog();
        progressDialogFragment.setArguments(bundle);
        return progressDialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.app.ProgressDialog progressDialog = new android.app.ProgressDialog(mContext);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(MESSAGE)) {
            String message = bundle.getString(MESSAGE);
            progressDialog.setMessage(!TextUtils.isEmpty(message) ? message : mContext.getString(R.string.text_loading));
        }

        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    /* --------------------------------------------------- */
    /* > Convenience */
    /* --------------------------------------------------- */

    public ProgressDialog uncancelable() {
        setCancelable(false);
        return this;
    }

    public ProgressDialog show(FragmentManager fragmentManager) {
        fragmentManager.beginTransaction()
                .add(this, ProgressDialog.class.getSimpleName())
                .commitAllowingStateLoss();
        return this;
    }

    public void dismiss() {
        dismissAllowingStateLoss();
    }
}

