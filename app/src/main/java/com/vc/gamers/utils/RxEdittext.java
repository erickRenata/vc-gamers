package com.vc.gamers.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import rx.Observable;
import rx.android.MainThreadSubscription;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class RxEdittext {

    public static Observable<String> bind(EditText editText) {

        return Observable.create(subscriber -> {

            TextWatcher textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    subscriber.onNext(String.valueOf(s));
                }
            };

            editText.addTextChangedListener(textWatcher);

            subscriber.add(new MainThreadSubscription() {
                @Override
                protected void onUnsubscribe() {
                    editText.removeTextChangedListener(textWatcher);
                }
            });

        });


    }
}