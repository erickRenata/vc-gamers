package com.vc.gamers.utils.dialog;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.vc.gamers.R;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class MessageDialog {

    public static void showMessage(Context context, int message) {
        showMessage(context, context.getString(message));
    }

    public static void showMessage(Context context, String message) {
        new MaterialDialog.Builder(context)
                .content(message)
                .positiveText(R.string.ok)
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();
    }
}
