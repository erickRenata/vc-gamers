package com.vc.gamers.utils.handler;

import android.content.Intent;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vc.gamers.VCGamersApp;
import com.vc.gamers.base.BaseResponse;
import com.vc.gamers.persistence.Preferences;
import com.vc.gamers.ui.login.LoginAct;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class RxUtils {

    public static <T> Observable.Transformer<T, T> applyScheduler() {
        return tObservable -> tObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> Observable.Transformer<T, T> applyApiCall() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(throwable -> {

                    if (throwable instanceof HttpException) {
                        try {
                            HttpException httpException = (HttpException) throwable;
                            String res = httpException.response().errorBody().string();

                            Gson gson = new Gson();
                            if (res.equalsIgnoreCase("Unauthorized.")){
                                BaseResponse response = new BaseResponse();
                                response.setMessage(res);
                                NetworkException e = new NetworkException(response, httpException.response().code());

                                VCGamersApp.getContext().startActivity(new Intent(VCGamersApp.getContext(), LoginAct.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                                // TODO : remove all memory
                                Preferences.removeLoginResponse();
                                Preferences.removeProfileResponse();
                                Preferences.removeRememberMe();
                                return Observable.error(e);
                            }else {
                                BaseResponse baseResponse = gson.fromJson(res, BaseResponse.class);
                                NetworkException e = new NetworkException(baseResponse, httpException.response().code());
                                if (e.getResponseCode() != 200){
                                    Toast.makeText(VCGamersApp.getContext(), e.getResponse().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                                return Observable.error(e);
                            }
                        } catch (IOException e) {
                            return Observable.empty();
                        }
                    } else
                        return Observable.empty();
                });
    }
}
