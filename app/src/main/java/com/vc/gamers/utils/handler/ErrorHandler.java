package com.vc.gamers.utils.handler;

import com.vc.gamers.base.BaseResponse;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class ErrorHandler {

    public static String handleError(Throwable throwable) {

        if (throwable == null) return "Connection Problem";
        if (throwable instanceof NetworkException) {
            NetworkException networkException = (NetworkException) throwable;
//            BaseResponse baseResponse = networkException.getError();
//            ErrorResponse baseResponse = networkException.getResponse();
            return networkException.getMessage();
        }
        return "Connection Problem";
    }
}

