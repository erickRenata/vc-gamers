package com.vc.gamers.listener;

/**
 * Created by @erickrenata on 2020-01-27.
 */

public interface OnFilterSelectedListener {
    void onItemFilterSelected(int type, int position);
}
