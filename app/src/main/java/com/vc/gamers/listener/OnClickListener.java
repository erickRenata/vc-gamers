package com.vc.gamers.listener;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public interface OnClickListener {
    void onItemClicked(int position);
}
