package com.vc.gamers.persistence;

import com.orhanobut.hawk.Hawk;
import com.vc.gamers.model.response.login.Login;
import com.vc.gamers.model.response.product.ModelText;
import com.vc.gamers.model.response.product.RememberMe;
import com.vc.gamers.model.response.product.Type;
import com.vc.gamers.model.response.profile.Profile;

import java.util.List;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class Preferences {

    public class Key {
        static final String LOGIN = "Key.Login";
        static final String PROFILE = "Key.Profile";

        static final String FILTER_TYPE = "Key.Type";
        static final String FILTER_CATEGORY = "Key.Category";
        static final String FILTER_GENRE = "Key.Genre";

        static final String REMEMBER_ME = "Key.RememberMe";
    }

    /* Login */
    public static void saveLoginResponse(Login login) {
        Hawk.put(Key.LOGIN, login);
    }

    public static Login getLoginResponse() {
        return Hawk.get(Key.LOGIN);
    }

    public static void removeLoginResponse(){
        Hawk.delete(Key.LOGIN);
    }

    /* Profile */
    public static void saveProfileRespose(Profile profile) {
        Hawk.put(Key.PROFILE, profile);
    }

    public static Profile getProfileResponse() {
        return Hawk.get(Key.PROFILE);
    }

    public static void removeProfileResponse(){
        Hawk.delete(Key.PROFILE);
    }

    /* Filter Type */
    public static void saveFilterType(List<ModelText> typeList) {
        Hawk.put(Key.FILTER_TYPE, typeList);
    }

    public static List<ModelText> getFilterType() {
        return Hawk.get(Key.FILTER_TYPE);
    }

    /* Filter Category */
    public static void saveFilterCategory(List<ModelText> categoryList) {
        Hawk.put(Key.FILTER_CATEGORY, categoryList);
    }

    public static List<ModelText> getFilterCategory() {
        return Hawk.get(Key.FILTER_CATEGORY);
    }

    /* Filter Genre */
    public static void saveFilterGenre(List<ModelText> genreList) {
        Hawk.put(Key.FILTER_GENRE, genreList);
    }

    public static List<ModelText> getFilterGenre() {
        return Hawk.get(Key.FILTER_GENRE);
    }

    /* Remember Me */
    public static void saveRememberMe(List<RememberMe> rememberMeList) {
        Hawk.put(Key.REMEMBER_ME, rememberMeList);
    }

    public static List<RememberMe> getRememberMe() {
        return Hawk.get(Key.REMEMBER_ME);
    }

    public static void removeRememberMe(){
        Hawk.delete(Key.REMEMBER_ME);
    }

}

