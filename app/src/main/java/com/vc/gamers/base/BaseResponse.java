package com.vc.gamers.base;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class BaseResponse {

    private String message;
    private boolean email_registered;

    public String getMessage() {
        return message;
    }

    public void setMessage(String meta) {
        this.message = meta;
    }

    public boolean isEmail_registered() {
        return email_registered;
    }

    public void setEmail_registered(boolean email_registered) {
        this.email_registered = email_registered;
    }
}

