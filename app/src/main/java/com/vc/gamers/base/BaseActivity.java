package com.vc.gamers.base;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.vc.gamers.utils.dialog.ProgressDialog;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog = ProgressDialog.create();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        progressDialog = ProgressDialog.create();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public void setStatusBarColor(int color) {
//        Window window = getWindow();
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        window.setStatusBarColor(ContextCompat.getColor(this, color));
    }

    public FragmentManager getBaseFragmentManager() {
        return getSupportFragmentManager();
    }

    public BaseFragment getDisplayedFragment(int container) {
        try {
            return ((BaseFragment) getSupportFragmentManager().findFragmentById(container));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void replaceFragment(int container, BaseFragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(container, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void removeFragment(BaseFragment fragment) {
        try {
            getSupportFragmentManager().beginTransaction()
                    .detach(fragment)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    protected void initStandardActionBar(Toolbar toolbar, String title) {
//        this.setSupportActionBar(toolbar);
//        ActionBar actionBar = this.getSupportActionBar();
//        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        actionBar.setDisplayShowCustomEnabled(true);
//        actionBar.setCustomView(R.layout.standard_action_bar);
////        getSupportActionBar().setElevation(0);
//        View view = actionBar.getCustomView();
//        ImageView btnBack = view.findViewById(R.id.btn_back);
//        TextView tvTitle = view.findViewById(R.id.tv_title);
//        tvTitle.setText(title);
//
//        btnBack.setOnClickListener(view1 -> onBackPressed());
//    }

    public void showLoading() {
        progressDialog.show(getBaseFragmentManager());
    }

    public void dismissLoading() {
        progressDialog.dismiss();
    }

    /***** Hides and show the soft keyboard *****/
    public void showSoftKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /***** Check connectivity *****/
    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo ni = cm.getActiveNetworkInfo();
            return ni != null;
        } else {
            return false;
        }
    }
}