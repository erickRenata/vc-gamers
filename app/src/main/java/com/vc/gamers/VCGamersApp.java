package com.vc.gamers;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class VCGamersApp extends Application {

    private static VCGamersApp instance;

    public VCGamersApp() {
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        setupHawk();
        setupCalligraphy();
    }

    private void setupHawk() {
        Hawk.init(this)
                .setEncryption(new NoEncryption())
                .build();
    }

    private void setupCalligraphy() {
//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/Flexo_Regular.otf")
//                .setFontAttrId(R.attr.fontPath)
//                .build());
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                                new CalligraphyConfig.Builder()
                                        .setDefaultFontPath("fonts/Saira_Regular.ttf")
                                        .setFontAttrId(R.attr.fontPath)
                                        .build()
                        )
                )
                .build()
        );
    }
}