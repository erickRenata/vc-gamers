package com.vc.gamers.widget;

import android.graphics.drawable.GradientDrawable;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class CustomColorDrawable extends GradientDrawable {

    public CustomColorDrawable(int pColor) {
        super(Orientation.BOTTOM_TOP,new int[]{pColor,pColor,pColor});
        setShape(GradientDrawable.RECTANGLE);
        setCornerRadius(50);
    }
}
