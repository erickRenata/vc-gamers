package com.vc.gamers.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.history.Order;
import com.vc.gamers.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class PurchasedHolder extends BaseHolder {

    @BindView(R.id.tv_purchased_game)
    public TextView tvPurchasedGame;
    @BindView(R.id.tv_purchased_price)
    public TextView tvPurchasedPrice;
    @BindView(R.id.tv_purchased_date)
    public TextView tvPurchasedDate;
    @BindView(R.id.iv_purchased)
    public ImageView ivPurchased;
    @BindView(R.id.tv_transaction_id)
    public TextView tvTransactionId;

    public PurchasedHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public void initView(Order order) {
        tvTransactionId.setText(order.getId());
        Picasso.get().load(order.getImage())
                .placeholder(R.drawable.ic_placeholder_image)
                .into(ivPurchased);
        tvPurchasedGame.setText(order.getProduct_name() + " - " + order.getLabel());
//        tvPurchasedPrice.setText(order.getAmount_in_currency());
        tvPurchasedPrice.setText(Utils.convertNumberWithRp(order.getAmount()));
        tvPurchasedDate.setText(Utils.formatDate(order.getDate()));
    }
}
