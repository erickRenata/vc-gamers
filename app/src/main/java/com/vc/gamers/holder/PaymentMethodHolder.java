package com.vc.gamers.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.payment.PaymentMethod;
import com.vc.gamers.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2020-01-30.
 */

public class PaymentMethodHolder extends BaseHolder {

    @BindView(R.id.iv_logo_payment)
    public ImageView ivLogoPayment;
    @BindView(R.id.tv_payment_method)
    public TextView tvPaymentMethod;
    @BindView(R.id.tv_payment_price)
    public TextView tvPaymentPrice;
    @BindView(R.id.tv_promo)
    public TextView tvPromo;
    @BindView(R.id.lin_payment_method)
    public LinearLayout linPaymentMethod;

    public PaymentMethodHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);
    }

    public void initView(Context mContext, PaymentMethod payment, Denomination denominationSelected) {
        tvPaymentMethod.setText(payment.getName());
        Picasso.get().load(payment.getImage()).into(ivLogoPayment);
        tvPromo.setVisibility(View.GONE);
        if (payment.getPaymentDenomination() != null) {
            tvPaymentPrice.setText(Utils.convertNumberWithRp(payment.getPaymentDenomination().getPrice_item()));

//            if (payment.getPaymentDenomination().getDiscount() > 0) {
            if (denominationSelected.getDiscount() > 0) {
                tvPromo.setVisibility(View.VISIBLE);
            }
        }

        if (!payment.isSelected()) {
            linPaymentMethod.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_radius_white_stroke_gray_6));
            tvPaymentMethod.setTextColor(ContextCompat.getColor(mContext, R.color.colorTextBlack));
            tvPaymentPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorPurple));
        } else {
            linPaymentMethod.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_radius_white_stroke_accent));
            tvPaymentMethod.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            tvPaymentPrice.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
        }
    }
}

