package com.vc.gamers.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.game.Product;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class TopGamesHolder extends BaseHolder {

    @BindView(R.id.tv_games_title)
    public TextView tvGamesTitle;
    @BindView(R.id.tv_games_creator)
    public TextView tvGamesCreator;
    @BindView(R.id.iv_games)
    public ImageView ivGames;

    @BindView(R.id.v_divider)
    public View vDivider;

    public TopGamesHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public void initView(Context mContext, Product product) {
        Picasso.get().load(product.getImage())
                .placeholder(R.drawable.ic_placeholder_image)
                .into(ivGames);
        tvGamesTitle.setText(product.getName());
        tvGamesCreator.setText(product.getCompany());
    }
}

