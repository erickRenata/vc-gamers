package com.vc.gamers.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.game.Product;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class GamesVoucherHolder extends BaseHolder {

    @BindView(R.id.tv_games_vouchers_title)
    public TextView tvGamesVouchersTitle;
    @BindView(R.id.tv_games_vouchers_creator)
    public TextView tvGamesVouchersCreator;
    @BindView(R.id.iv_games_vouchers)
    public ImageView ivGamesVouchers;

    public GamesVoucherHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public void initView(Product product) {
        Picasso.get().load(product.getImage())
                .placeholder(R.drawable.ic_placeholder_image)
                .into(ivGamesVouchers);
        tvGamesVouchersTitle.setText(product.getName());
        tvGamesVouchersCreator.setText(product.getCompany());
    }
}

