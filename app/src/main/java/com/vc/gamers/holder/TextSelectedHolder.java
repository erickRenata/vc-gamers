package com.vc.gamers.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.product.ModelText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class TextSelectedHolder extends BaseHolder {

    @BindView(R.id.tv_selected_text)
    public TextView tvSelectedText;
    @BindView(R.id.v_divider_left)
    public View vDividerLeft;
    @BindView(R.id.v_divider_right)
    public View vDividerRight;

    public TextSelectedHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public void initView(ModelText modelText, Context mContext) {
        tvSelectedText.setText(modelText.getName());

        if (modelText.isSelected()) {
            tvSelectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            tvSelectedText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_radius_blue_stroke_blue_6));
        } else {
            tvSelectedText.setTextColor(ContextCompat.getColor(mContext, R.color.colorTextBlack));
            tvSelectedText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_radius_white_stroke_gray_6));
        }
    }
}
