package com.vc.gamers.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.adapter.PaymentMethodAdapter;
import com.vc.gamers.adapter.PaymentMethodListAdapter;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.payment.Payment;
import com.vc.gamers.model.response.payment.PaymentMethod;
import com.vc.gamers.ui.topup.TopUpFragmentStep3;
import com.vc.gamers.widget.AnimView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2020-01-30.
 */

public class PaymentMethodListHolder extends BaseHolder {

    @BindView(R.id.iv_payment_method)
    public ImageView ivPaymentMethod;
    @BindView(R.id.tv_payment_method)
    public TextView tvPaymentMethod;
    @BindView(R.id.tv_promo)
    public TextView tvPromo;
    @BindView(R.id.iv_arrow)
    public ImageView ivArrow;
    @BindView(R.id.lin_payment_method)
    public LinearLayout linPaymentMethod;

    @BindView(R.id.rv_content)
    public RecyclerView rvContent;

    public PaymentMethodAdapter topUpPaymentAdapter;
    public List<PaymentMethod> methodList = new ArrayList<>();

    private Context mContext;
    private Payment payment;

    public PaymentMethodListHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);
    }

    public void initView(Context mContext, Payment payment, PaymentMethodListAdapter paymentMethodListAdapter, int position, TopUpFragmentStep3 fragment, Denomination denominationSelected) {
        this.mContext = mContext;
        this.payment = payment;
        tvPaymentMethod.setText(payment.getName());
        Picasso.get().load(payment.getImage()).into(ivPaymentMethod);

        methodList.clear();
        methodList.addAll(payment.getMethods());

        tvPromo.setVisibility(View.GONE);
//        boolean isFound = false;
//        for (PaymentMethod paymentMethod: methodList){
//            if (paymentMethod.getPaymentDenomination() != null){
//                if (paymentMethod.getPaymentDenomination().getDiscount() > 0){
//                    isFound = true;
//                    break;
//                }
//            }
//        }
//        if (isFound){
//            tvPromo.setVisibility(View.VISIBLE);
//        }
        if (denominationSelected.getDiscount() > 0){
            tvPromo.setVisibility(View.VISIBLE);
        }

        topUpPaymentAdapter = new PaymentMethodAdapter(mContext, methodList, paymentMethodListAdapter, position, fragment, denominationSelected);
        rvContent.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        rvContent.setAdapter(topUpPaymentAdapter);

//        boolean isFound = false;
//        for (PaymentMethod paymentMethod: payment.getMethods()){
//            if (paymentMethod.isSelected()){
//                isFound = true;
//                break;
//            }
//        }
//        if (isFound){
//            return;
//        }
//
//        if (payment.isSelected()) {
////            if (linPaymentMethod.getVisibility() == View.VISIBLE) {
////                ivArrow.animate().rotation(-90f);
////                AnimView.collapse(linPaymentMethod);
////            }else {
//                ivArrow.animate().rotation(0f);
//                AnimView.expand(linPaymentMethod);
////            }
//        } else {
//            if (linPaymentMethod.getVisibility() == View.VISIBLE) {
//                ivArrow.animate().rotation(-90f);
//                AnimView.collapse(linPaymentMethod);
//            }
//        }
    }
}


