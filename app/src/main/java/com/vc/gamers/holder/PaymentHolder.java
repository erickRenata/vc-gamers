package com.vc.gamers.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.payment.Payment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2020-01-10.
 */

public class PaymentHolder extends BaseHolder {

    @BindView(R.id.tv_payment_type)
    public TextView tvPaymentType;
    @BindView(R.id.iv_payment_selected)
    public ImageView ivPaymentSelected;

    public PaymentHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public void initView(Payment payment) {
        tvPaymentType.setText(payment.getName());

        ivPaymentSelected.setVisibility(payment.isSelected() ? View.VISIBLE : View.INVISIBLE);
    }
}

