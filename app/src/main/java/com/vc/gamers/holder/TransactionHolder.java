package com.vc.gamers.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.history.Order;
import com.vc.gamers.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2019-12-13.
 */

public class TransactionHolder extends BaseHolder {

    @BindView(R.id.tv_transaction)
    public TextView tvTransaction;
    @BindView(R.id.tv_transaction_date)
    public TextView tvTransactionDate;
    @BindView(R.id.tv_transaction_price)
    public TextView tvTransactionPrice;
    @BindView(R.id.iv_transaction)
    public ImageView ivTransaction;
    @BindView(R.id.tv_transaction_id)
    public TextView tvTransactionId;

    @BindView(R.id.v_divider)
    public View vDivider;

    public TransactionHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public void initView(Order order) {
        tvTransactionId.setText(order.getId());
        tvTransaction.setText(order.getProduct_name() + " - " + order.getLabel());
        tvTransactionPrice.setText(Utils.convertNumberWithRp(order.getAmount()));
        tvTransactionDate.setText(Utils.formatDate(order.getDate()));
    }
}



