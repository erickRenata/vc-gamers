package com.vc.gamers.holder;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.article.Article;
import com.vc.gamers.utils.Utils;
import com.vc.gamers.widget.CustomColorDrawable;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class NewsPromosHolder extends BaseHolder {

    @BindView(R.id.tv_news_promo_category)
    public TextView tvNewsPromoCategory;
    @BindView(R.id.tv_news_promo_title)
    public TextView tvNewsPromoTitle;
    @BindView(R.id.tv_news_promo_date)
    public TextView tvNewsPromoDate;
    @BindView(R.id.iv_news_promo)
    public ImageView ivNewsPromo;

    public NewsPromosHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);
    }

    public void initView(Article article) {
        if (article.getImage() != null) {
            if (!article.getImage().isEmpty()) {
                Picasso.get().load(article.getImage())
                        .placeholder(R.drawable.ic_placeholder_image)
                        .into(ivNewsPromo);
            }
        }
        tvNewsPromoCategory.setText(Utils.toTitleCase(article.getType()));
        int color;
        if (article.getType().equalsIgnoreCase("news")) {
            color = Color.parseColor("#00A3DB");
        } else {
            color = Color.parseColor("#9A248E");
        }
//        ((GradientDrawable) tvNewsPromoCategory.getBackground()).setColor(color);
        CustomColorDrawable drawable = new CustomColorDrawable(color);
        tvNewsPromoCategory.setBackgroundDrawable(drawable);
        tvNewsPromoTitle.setText(article.getTitle());
        tvNewsPromoDate.setText(Utils.formatDate(article.getCreated_at()));
    }
}

