package com.vc.gamers.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.game.Product;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class VouchersHolder extends BaseHolder {

    @BindView(R.id.tv_vouchers_title)
    public TextView tvGamesVouchersTitle;
    @BindView(R.id.tv_vouchers_creator)
    public TextView tvGamesVouchersCreator;
    @BindView(R.id.iv_vouchers)
    public ImageView ivGamesVouchers;

    @BindView(R.id.v_divider)
    public View vDivider;

    public VouchersHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public void initView(Context mContext, Product product) {
        Picasso.get().load(product.getImage())
                .placeholder(R.drawable.ic_placeholder_image)
                .into(ivGamesVouchers);
        tvGamesVouchersTitle.setText(product.getName());
        tvGamesVouchersCreator.setText(product.getCompany());
    }
}


