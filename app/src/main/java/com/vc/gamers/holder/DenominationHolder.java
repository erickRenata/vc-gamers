package com.vc.gamers.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseHolder;
import com.vc.gamers.model.response.game.Denomination;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2020-01-30.
 */

public class DenominationHolder extends BaseHolder {

    @BindView(R.id.tv_denomination)
    public TextView tvDenomination;

    public DenominationHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);

    }

    public void initView(Context mContext, Denomination denomination) {
        tvDenomination.setText(denomination.getAmount_in_currency());

        if (denomination.isIs_selected()) {
            tvDenomination.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            tvDenomination.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_radius_blue_stroke_blue_6));
        } else {
            tvDenomination.setTextColor(ContextCompat.getColor(mContext, R.color.colorTextBlack));
            tvDenomination.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_radius_white_stroke_gray_6));
        }
    }
}


