package com.vc.gamers.presenter;

import com.vc.gamers.model.response.ResponseList;
import com.vc.gamers.model.response.ResponseObject;
import com.vc.gamers.model.response.article.Article;
import com.vc.gamers.network.ApiService;
import com.vc.gamers.network.NetworkModule;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by @erickrenata on 2020-01-10.
 */

public class ArticlePresenter {

    public static Observable<ResponseList<Article>> getArticle() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getArticle()
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseObject<Article>> getDetailArticle(String id) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getDetailArticle(id)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseList<Article>> getNewsList() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getNewsList()
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseList<Article>> getPromoList() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getPromoList()
                .subscribeOn(Schedulers.computation());
    }
}
