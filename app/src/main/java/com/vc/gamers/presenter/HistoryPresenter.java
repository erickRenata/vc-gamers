package com.vc.gamers.presenter;

import com.vc.gamers.model.response.ResponseList;
import com.vc.gamers.model.response.history.Order;
import com.vc.gamers.network.ApiService;
import com.vc.gamers.network.NetworkModule;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by @erickrenata on 2020-01-20.
 */

public class HistoryPresenter {

    public static Observable<ResponseList<Order>> getPurchasedOrder() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getPurchasedOrder()
                .subscribeOn(Schedulers.computation());
    }
}
