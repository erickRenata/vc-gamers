package com.vc.gamers.presenter;

import com.vc.gamers.base.BaseResponse;
import com.vc.gamers.model.response.ResponseObject;
import com.vc.gamers.model.response.login.Login;
import com.vc.gamers.model.response.profile.Profile;
import com.vc.gamers.network.ApiService;
import com.vc.gamers.network.NetworkModule;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.schedulers.Schedulers;

import static com.vc.gamers.constant.Constant.CLIENT_ID;
import static com.vc.gamers.constant.Constant.CLIENT_SECRET;
import static com.vc.gamers.constant.Constant.GRANT_TYPE;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class UserPresenter {

    public static Observable<Login> postLogin(String username, String password) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .postLogin(GRANT_TYPE, CLIENT_ID, CLIENT_SECRET, username, password)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<Login> postLoginFacebook(String email, String provider,
                                                      String authToken, String idToken,
                                                      String photoUrl, String name) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .postLoginFacebook(email, provider, authToken, idToken, photoUrl, name)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<Login> postRegister(String name, String email, String password, String securityCode) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .postRegister(email, name, password, securityCode)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseObject<Profile>> getProfile() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getProfile()
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<BaseResponse> postUpdatePassword(String oldPassword, String newPassword, String confirmPassword) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .postUpdatePassword(oldPassword, newPassword, confirmPassword)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<BaseResponse> postForgotPassword(String email) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .postForgotPassword(email)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<BaseResponse> postUpdateProfile(File file, String name, String email, String phoneNumber, String birthDate) {
        if (file != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            RequestBody reqName = RequestBody.create(MediaType.parse("text/plain"), name);
            RequestBody reqEmail = RequestBody.create(MediaType.parse("text/plain"), email);
            RequestBody reqPhoneNumber = RequestBody.create(MediaType.parse("text/plain"), phoneNumber);
            RequestBody reqBirthDate = RequestBody.create(MediaType.parse("text/plain"), birthDate);
            MultipartBody.Part image = MultipartBody.Part.createFormData("avatar", file.getName(), reqFile);

            return NetworkModule.provideRetrofit()
                    .create(ApiService.class)
                    .postUpdateProfile(reqName, reqEmail, reqPhoneNumber, reqBirthDate, image)
                    .subscribeOn(Schedulers.computation());
        } else {
            return NetworkModule.provideRetrofit()
                    .create(ApiService.class)
                    .postUpdateProfile(name, email, phoneNumber, birthDate)
                    .subscribeOn(Schedulers.computation());
        }
    }
}
