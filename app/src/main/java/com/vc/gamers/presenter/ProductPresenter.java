package com.vc.gamers.presenter;

import com.vc.gamers.model.response.ResponseList;
import com.vc.gamers.model.response.ResponseObject;
import com.vc.gamers.model.response.game.DetailProduct;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.model.response.product.Category;
import com.vc.gamers.model.response.product.Genre;
import com.vc.gamers.model.response.product.Type;
import com.vc.gamers.network.ApiService;
import com.vc.gamers.network.NetworkModule;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by @erickrenata on 2020-01-20.
 */

public class ProductPresenter {

    public static Observable<ResponseList<Product>> getProductSearch(String name) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getProductSearch(name)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseList<Product>> getProductList(String page,
                                                                   String type,
                                                                   String categoryId,
                                                                   String genreId) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getProductList(page, "name", type, "DESC", categoryId, genreId)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseList<Product>> getProductListAll(String page) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getProductListAll(page, "name", "DESC")
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseList<Product>> getProductTopList() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getProductTopList()
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseObject<DetailProduct>> getDetailProduct(String idGame) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getDetailProduct(idGame)
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseList<Type>> getProductType() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getProductType()
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseList<Category>> getProductCategory() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getProductCategory()
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseList<Genre>> getProductGenre() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getProductGenre()
                .subscribeOn(Schedulers.computation());
    }
}
