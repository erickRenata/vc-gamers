package com.vc.gamers.presenter;

import com.vc.gamers.model.response.ResponseList;
import com.vc.gamers.model.response.banner.Banner;
import com.vc.gamers.network.ApiService;
import com.vc.gamers.network.NetworkModule;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by @erickrenata on 2020-01-20.
 */

public class BannerPresenter {

    public static Observable<ResponseList<Banner>> getBannerList() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getBannerList()
                .subscribeOn(Schedulers.computation());
    }
}
