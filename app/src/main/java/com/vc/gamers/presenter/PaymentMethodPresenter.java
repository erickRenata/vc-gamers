package com.vc.gamers.presenter;

import com.vc.gamers.base.BaseResponse;
import com.vc.gamers.model.response.ResponseList;
import com.vc.gamers.model.response.ResponseObject;
import com.vc.gamers.model.response.checkout.Checkout;
import com.vc.gamers.model.response.history.Order;
import com.vc.gamers.model.response.payment.Payment;
import com.vc.gamers.network.ApiService;
import com.vc.gamers.network.NetworkModule;

import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by @erickrenata on 2020-01-10.
 */

public class PaymentMethodPresenter {

    public static Observable<ResponseList<Payment>> getPaymentMethodList() {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .getPaymentMethodList()
                .subscribeOn(Schedulers.computation());
    }

    public static Observable<ResponseObject<Checkout>> postCheckout(String denomination_id, String product_name, String product_type,
                                                                    String amount, String amount_in_currency, String subtotal,
                                                                    String fee, String grand_total, String reward, String payment_id,
                                                                    String payment_name, String payment_type, String payment_method_category_id,
                                                                    String user_player_id, String additional_information, String email, String phoneNumber) {
        return NetworkModule.provideRetrofit()
                .create(ApiService.class)
                .postCheckout(denomination_id, product_name, product_type, amount, amount_in_currency, subtotal, fee, grand_total, reward, payment_id, payment_name, payment_type, payment_method_category_id, user_player_id, additional_information, email, phoneNumber)
                .subscribeOn(Schedulers.computation());
    }
}
