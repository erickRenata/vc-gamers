package com.vc.gamers.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.holder.GamesVoucherHolder;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.ui.game.DetailGameAct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class GamesVouchersAdapter extends RecyclerView.Adapter<GamesVoucherHolder> {

    private List<Product> gamesVouchersList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    public GamesVouchersAdapter(Context mContext, List<Product> gamesVouchersList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.gamesVouchersList.clear();
        this.gamesVouchersList.addAll(gamesVouchersList);
    }

    public void replaceItem(List<Product> vouchers) {
        this.gamesVouchersList.clear();
        this.gamesVouchersList.addAll(vouchers);
        notifyDataSetChanged();
    }

    @Override
    public GamesVoucherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_games_vouchers, parent, false);
        return new GamesVoucherHolder(view);
    }

    @Override
    public void onBindViewHolder(final GamesVoucherHolder holder, final int position) {
        holder.initView(gamesVouchersList.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, DetailGameAct.class)
                        .putExtra(Product.class.getSimpleName(), gamesVouchersList.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return gamesVouchersList.size();
    }
}
