package com.vc.gamers.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.holder.PurchasedHolder;
import com.vc.gamers.model.response.history.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class PurchasedAdapter extends RecyclerView.Adapter<PurchasedHolder> {

    private List<Order> purchasedList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    public PurchasedAdapter(Context mContext, List<Order> purchasedList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.purchasedList.clear();
        this.purchasedList.addAll(purchasedList);
    }

    public void replaceItem(List<Order> purchasedList) {
        this.purchasedList.clear();
        this.purchasedList.addAll(purchasedList);
        notifyDataSetChanged();
    }

    @Override
    public PurchasedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_purchased, parent, false);
        return new PurchasedHolder(view);
    }

    @Override
    public void onBindViewHolder(final PurchasedHolder holder, final int position) {
        holder.initView(purchasedList.get(position));
    }

    @Override
    public int getItemCount() {
        return purchasedList.size();
    }
}