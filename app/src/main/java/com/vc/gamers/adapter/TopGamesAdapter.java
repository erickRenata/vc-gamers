package com.vc.gamers.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.holder.TopGamesHolder;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.ui.game.DetailGameAct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class TopGamesAdapter extends RecyclerView.Adapter<TopGamesHolder> {

    private List<Product> topGamesList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    public TopGamesAdapter(Context mContext, List<Product> topGamesList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.topGamesList.clear();
        this.topGamesList.addAll(topGamesList);
    }

    public void replaceItem(List<Product> topGamesList) {
        this.topGamesList.clear();
        this.topGamesList.addAll(topGamesList);
        notifyDataSetChanged();
    }

    @Override
    public TopGamesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_top_up, parent, false);
        return new TopGamesHolder(view);
    }

    @Override
    public void onBindViewHolder(final TopGamesHolder holder, final int position) {
        holder.initView(mContext, topGamesList.get(position));

        holder.vDivider.setVisibility(View.GONE);
        if (position == topGamesList.size() - 1) {
            holder.vDivider.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, DetailGameAct.class)
                        .putExtra(Product.class.getSimpleName(), topGamesList.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return topGamesList.size();
    }
}
