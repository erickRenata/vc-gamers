package com.vc.gamers.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.holder.TopGamesHolder;
import com.vc.gamers.holder.VouchersHolder;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.ui.game.DetailGameAct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class VouchersAdapter extends RecyclerView.Adapter<VouchersHolder> {

    private List<Product> voucherList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    public VouchersAdapter(Context mContext, List<Product> voucherList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.voucherList.clear();
        this.voucherList.addAll(voucherList);
    }

    public void replaceItem(List<Product> vouchers) {
        this.voucherList.clear();
        this.voucherList.addAll(vouchers);
        notifyDataSetChanged();
    }

    @Override
    public VouchersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_vouchers, parent, false);
        return new VouchersHolder(view);
    }

    @Override
    public void onBindViewHolder(final VouchersHolder holder, final int position) {
        holder.initView(mContext, voucherList.get(position));

        holder.vDivider.setVisibility(View.GONE);
        if (position == voucherList.size() - 1){
            holder.vDivider.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, DetailGameAct.class)
                        .putExtra(Product.class.getSimpleName(), voucherList.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return voucherList.size();
    }
}
