package com.vc.gamers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.holder.PaymentMethodHolder;
import com.vc.gamers.listener.OnClickListener;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.payment.PaymentMethod;
import com.vc.gamers.ui.topup.NewTopUpAct;
import com.vc.gamers.ui.topup.TopUpFragmentStep3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2020-01-30.
 */

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodHolder> {

    private final int position;
    private final TopUpFragmentStep3 fragment;
    private final Denomination denominationSelected;
    private List<PaymentMethod> paymentList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;
    private PaymentMethodListAdapter paymentMethodListAdapter;

    private OnClickListener mOnClickListener;

    public PaymentMethodAdapter(Context mContext, List<PaymentMethod> paymentList, PaymentMethodListAdapter paymentMethodListAdapter, int position, TopUpFragmentStep3 fragment, Denomination denominationSelected) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.paymentList.clear();
        this.paymentList.addAll(paymentList);
        this.fragment = fragment;
        this.paymentMethodListAdapter = paymentMethodListAdapter;
        this.position = position;
        this.denominationSelected = denominationSelected;
//        this.mOnClickListener = (OnClickListener) fragment;
    }

    public void replaceItem(List<PaymentMethod> paymentList) {
        this.paymentList.clear();
        this.paymentList.addAll(paymentList);
        notifyDataSetChanged();
    }

    @Override
    public PaymentMethodHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_payment_method, parent, false);
        return new PaymentMethodHolder(view);
    }

    @Override
    public void onBindViewHolder(final PaymentMethodHolder holder, final int pos) {
        holder.initView(mContext, paymentList.get(pos), denominationSelected);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentMethodListAdapter.clearAllFalse(position);
                for (PaymentMethod text : paymentList) {
                    text.setSelected(false);
                }
                paymentList.get(pos).setSelected(true);

//                paymentMethodListAdapter.refreshData(position);

                notifyDataSetChanged();

                if (fragment != null) {
                    fragment.onSelectedPaymentMethod(paymentList.get(pos));
                } else {
                    ((NewTopUpAct) mContext).onSelectedPaymentMethod(paymentList.get(pos));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }
}