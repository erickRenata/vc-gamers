package com.vc.gamers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.holder.TransactionHolder;
import com.vc.gamers.model.response.history.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2019-12-13.
 */

public class TransactionAdapter extends RecyclerView.Adapter<TransactionHolder> {

    private List<Order> transactionList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    public TransactionAdapter(Context mContext, List<Order> transactionList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.transactionList.clear();
        this.transactionList.addAll(transactionList);
    }

    public void replaceItem(List<Order> transactionList) {
        this.transactionList.clear();
        this.transactionList.addAll(transactionList);
        notifyDataSetChanged();
    }

    @Override
    public TransactionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_transaction, parent, false);
        return new TransactionHolder(view);
    }

    @Override
    public void onBindViewHolder(final TransactionHolder holder, final int position) {
        holder.initView(transactionList.get(position));

        holder.vDivider.setVisibility(View.GONE);
        if (position == transactionList.size() - 1){
            holder.vDivider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }
}
