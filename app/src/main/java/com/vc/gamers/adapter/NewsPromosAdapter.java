package com.vc.gamers.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.holder.NewsPromosHolder;
import com.vc.gamers.model.response.article.Article;
import com.vc.gamers.ui.newspromos.DetailPromoAct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class NewsPromosAdapter extends RecyclerView.Adapter<NewsPromosHolder> {

    private List<Article> newsPromosList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    public NewsPromosAdapter(Context mContext, List<Article> newsPromosList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.newsPromosList.clear();
        this.newsPromosList.addAll(newsPromosList);
    }

    public void replaceItem(List<Article> vouchers) {
        this.newsPromosList.clear();
        this.newsPromosList.addAll(vouchers);
        notifyDataSetChanged();
    }

    @Override
    public NewsPromosHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_news_promos, parent, false);
        return new NewsPromosHolder(view);
    }

    @Override
    public void onBindViewHolder(final NewsPromosHolder holder, final int position) {
        holder.initView(newsPromosList.get(position));

        holder.itemView.setOnClickListener(view -> {
            if (newsPromosList.get(position).getType().equalsIgnoreCase("promo")) {
                mContext.startActivity(new Intent(mContext, DetailPromoAct.class)
                        .putExtra(Article.class.getSimpleName(), newsPromosList.get(position)));
            } else {
                mContext.startActivity(new Intent(mContext, DetailPromoAct.class)
                        .putExtra(Article.class.getSimpleName(), newsPromosList.get(position)));
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
//                mContext.startActivity(browserIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsPromosList.size();
    }
}