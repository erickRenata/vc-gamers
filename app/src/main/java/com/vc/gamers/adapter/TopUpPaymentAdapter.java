package com.vc.gamers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.holder.PaymentHolder;
import com.vc.gamers.model.response.payment.Payment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2020-01-10.
 */

public class TopUpPaymentAdapter extends RecyclerView.Adapter<PaymentHolder> {

    private List<Payment> paymentList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    public TopUpPaymentAdapter(Context mContext, List<Payment> paymentList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.paymentList.clear();
        this.paymentList.addAll(paymentList);
    }

    public void replaceItem(List<Payment> paymentList) {
        this.paymentList.clear();
        this.paymentList.addAll(paymentList);
        notifyDataSetChanged();
    }

    @Override
    public PaymentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_payment, parent, false);
        return new PaymentHolder(view);
    }

    @Override
    public void onBindViewHolder(final PaymentHolder holder, final int position) {
        holder.initView(paymentList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < paymentList.size(); i++) {
                    paymentList.get(i).setSelected(false);
                }
                paymentList.get(position).setSelected(!paymentList.get(position).isSelected());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }
}