package com.vc.gamers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.holder.TextSelectedHolder;
import com.vc.gamers.listener.OnFilterSelectedListener;
import com.vc.gamers.model.response.product.ModelText;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class TextSelectedAdapter extends RecyclerView.Adapter<TextSelectedHolder> {

    public static final int PRODUCT_TYPE = 100;
    public static final int PRODUCT_CATEGORY = 200;
    public static final int ARTICLE_TYPE = 300;

    private List<ModelText> textList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    private OnFilterSelectedListener mOnFilterSelectedListener;

    public TextSelectedAdapter(Context mContext, List<ModelText> textList) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.textList.clear();
        this.textList.addAll(textList);
        mOnFilterSelectedListener = (OnFilterSelectedListener) mContext;
    }

    public void replaceItem(List<ModelText> textList) {
        this.textList.clear();
        this.textList.addAll(textList);
        notifyDataSetChanged();
    }

    @Override
    public TextSelectedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_selected_text, parent, false);
        return new TextSelectedHolder(view);
    }

    @Override
    public void onBindViewHolder(final TextSelectedHolder holder, final int position) {
        holder.initView(textList.get(position), mContext);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (textList.get(position).getType()) {
                    case PRODUCT_TYPE:
                        mOnFilterSelectedListener.onItemFilterSelected(PRODUCT_TYPE, position);
                        break;
                    case PRODUCT_CATEGORY:
                        mOnFilterSelectedListener.onItemFilterSelected(PRODUCT_CATEGORY, position);
                        break;
                    case ARTICLE_TYPE:
                        mOnFilterSelectedListener.onItemFilterSelected(ARTICLE_TYPE, position);
                        break;
                    default:
                        mOnFilterSelectedListener.onItemFilterSelected(0, position);
                        break;
                }
                for (ModelText text : textList) {
                    text.setSelected(false);
                }
                textList.get(position).setSelected(true);
                notifyDataSetChanged();
            }
        });

        holder.vDividerRight.setVisibility(View.GONE);
        holder.vDividerLeft.setVisibility(View.GONE);
        if (position == textList.size() - 1) {
            holder.vDividerRight.setVisibility(View.VISIBLE);
        }
        if (position == 0) {
            holder.vDividerLeft.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return textList.size();
    }
}

