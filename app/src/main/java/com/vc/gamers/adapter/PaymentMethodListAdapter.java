package com.vc.gamers.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.holder.PaymentMethodListHolder;
import com.vc.gamers.listener.OnClickListener;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.payment.Payment;
import com.vc.gamers.model.response.payment.PaymentMethod;
import com.vc.gamers.ui.topup.TopUpFragmentStep3;
import com.vc.gamers.widget.AnimView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2020-01-30.
 */

public class PaymentMethodListAdapter extends RecyclerView.Adapter<PaymentMethodListHolder> {

    private final TopUpFragmentStep3 fragment;
    private final Denomination denominationSelected;
    private List<Payment> paymentList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    private OnClickListener mOnClickListener;

    public PaymentMethodListAdapter(Context mContext, List<Payment> paymentList, BaseFragment fragment, Denomination denominationSelected) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.paymentList.clear();
        this.paymentList.addAll(paymentList);
        this.fragment = (TopUpFragmentStep3) fragment;
        this.denominationSelected = denominationSelected;
//        this.mOnClickListener = (OnClickListener) fragment;
    }

    public void replaceItem(List<Payment> paymentList) {
        this.paymentList.clear();
        this.paymentList.addAll(paymentList);
        notifyDataSetChanged();
    }

    public void clearAllFalse(int position) {
        for (Payment payment : paymentList) {
//            payment.setSelected(false);
            for (PaymentMethod paymentMethod : payment.getMethods()) {
                paymentMethod.setSelected(false);
            }
        }
//        paymentList.get(position).setSelected(true);
        notifyDataSetChanged();
    }

    public void refreshData(int position) {
//        paymentList.get(position).setSelected(true);
        for (Payment payment : paymentList) {
            for (PaymentMethod paymentMethod : payment.getMethods()) {
                paymentMethod.setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public PaymentMethodListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_payment_method_list, parent, false);
        return new PaymentMethodListHolder(view);
    }

    private boolean isExpandedView = false;

    @Override
    public void onBindViewHolder(final PaymentMethodListHolder holder, final int position) {
        holder.initView(mContext, paymentList.get(position), this, position, fragment, denominationSelected);
        if (paymentList.get(position).isSelected()) {
            holder.ivArrow.animate().rotation(0f);
            AnimView.expand(holder.linPaymentMethod);
        } else {
            holder.ivArrow.animate().rotation(-90f);
            AnimView.collapse(holder.linPaymentMethod);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isExpandedView) {
                    return;
                }
                isExpandedView = true;
                new Handler().postDelayed(() -> isExpandedView = false, 300);

//                for (Payment text : paymentList) {
//                    text.setSelected(false);
//                }
                paymentList.get(position).setSelected(!paymentList.get(position).isSelected());
//                notifyItemChanged(position);
                if (paymentList.get(position).isSelected()) {
                    holder.ivArrow.animate().rotation(0f);
                    AnimView.expand(holder.linPaymentMethod);
                } else {
                    holder.ivArrow.animate().rotation(-90f);
                    AnimView.collapse(holder.linPaymentMethod);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }
}