package com.vc.gamers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.holder.DenominationHolder;
import com.vc.gamers.listener.OnClickListener;
import com.vc.gamers.model.response.game.Denomination;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @erickrenata on 2020-01-29.
 */

public class DenominationAdapter extends RecyclerView.Adapter<DenominationHolder> {

    private List<Denomination> denominationList = new ArrayList<>();
    private LayoutInflater inflater;
    private Context mContext;

    private OnClickListener mOnClickListener;

    public DenominationAdapter(Context mContext, List<Denomination> denominations, BaseFragment fragment) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.denominationList.clear();
        this.denominationList.addAll(denominations);
        if (fragment == null){
            this.mOnClickListener = (OnClickListener) mContext;
        }else {
            this.mOnClickListener = (OnClickListener) fragment;
        }
    }

    public void replaceItem(List<Denomination> denominations) {
        this.denominationList.clear();
        this.denominationList.addAll(denominations);
        notifyDataSetChanged();
    }

    @Override
    public DenominationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_denomination, parent, false);
        return new DenominationHolder(view);
    }

    @Override
    public void onBindViewHolder(final DenominationHolder holder, final int position) {
        holder.initView(mContext, denominationList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Denomination text : denominationList) {
                    text.setIs_selected(false);
                }
                denominationList.get(position).setIs_selected(true);
                notifyDataSetChanged();

                mOnClickListener.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return denominationList.size();
    }
}