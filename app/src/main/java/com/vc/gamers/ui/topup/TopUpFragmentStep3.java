package com.vc.gamers.ui.topup;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.adapter.PaymentMethodListAdapter;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.game.PaymentDenomination;
import com.vc.gamers.model.response.payment.Payment;
import com.vc.gamers.model.response.payment.PaymentMethod;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2019-12-10.
 */

public class TopUpFragmentStep3 extends BaseFragment {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;

    @BindView(R.id.lin_step_2)
    LinearLayout linStep2;
    @BindView(R.id.lin_step_3)
    LinearLayout linStep3;

    private Context mContext;

    Denomination denominationSelected;
    List<Payment> paymentList = new ArrayList<>();
    private PaymentMethodListAdapter topUpPaymentAdapter;
    public PaymentMethod paymentMethod;

    public static TopUpFragmentStep3 newInstance() {
        Bundle args = new Bundle();
        TopUpFragmentStep3 fragment = new TopUpFragmentStep3();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_up_3, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (((TopUpAct) mContext).isVoucher) {
            linStep2.setVisibility(View.VISIBLE);
            linStep3.setVisibility(View.GONE);
        } else {
            linStep2.setVisibility(View.GONE);
            linStep3.setVisibility(View.VISIBLE);
        }
    }

    public void initData(List<Payment> paymentList, Denomination denominationSelected) {
//        this.paymentList = paymentList;
        this.paymentList.clear();
        this.denominationSelected = denominationSelected;

//        List<Payment> paymentsTemp = new ArrayList<>();
//        for (PaymentDenomination paymentDenomination : denominationSelected.getPayment()) {
//            for (Payment payment : paymentList) {
//                List<PaymentMethod> paymentMethodList = new ArrayList<>();
//                for (PaymentMethod paymentMethod : payment.getMethods()) {
//                    if (paymentMethod.getId() == paymentDenomination.getPayment_method_id()) {
//                        paymentMethod.setPaymentDenomination(paymentDenomination);
//
//                        paymentMethodList.add(paymentMethod);
//                    }
//                }
//                payment.setMethods(paymentMethodList);
//                paymentsTemp.add(payment);
//            }
//        }

        List<Payment> paymentsTemp = new ArrayList<>();
        for (Payment payment : paymentList) {
            List<PaymentMethod> paymentMethodList = new ArrayList<>();
            for (PaymentDenomination paymentDenomination : denominationSelected.getPayment()) {
                for (PaymentMethod paymentMethod : payment.getMethods()) {
                    if (paymentMethod.getId() == paymentDenomination.getId()){
                        paymentMethod.setPaymentDenomination(paymentDenomination);
                        paymentMethodList.add(paymentMethod);
                    }
                }
            }
            payment.setMethods(paymentMethodList);
            paymentsTemp.add(payment);
        }
        this.paymentList.addAll(paymentsTemp);

//        for (Payment payment : paymentsTemp) {
//            List<PaymentMethod> paymentMethodList = new ArrayList<>();
//            for (PaymentMethod paymentMethod : payment.getMethods()) {
//                if (paymentMethod.getPaymentDenomination() != null) {
//                    paymentMethodList.add(paymentMethod);
//                }
//            }
//            payment.setMethods(paymentMethodList);
//            if (payment.getMethods().size() > 0) {
//                this.paymentList.add(payment);
//            }
//        }

        initAdapter();
    }

    public void initAdapter() {
        topUpPaymentAdapter = new PaymentMethodListAdapter(mContext, paymentList, this, denominationSelected);
        rvContent.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        rvContent.setAdapter(topUpPaymentAdapter);
    }

    public void onSelectedPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_next)
    void nextClicked() {
        if (paymentMethod == null) {
            Toast.makeText(mContext, "Payment Method Required", Toast.LENGTH_SHORT).show();
            return;
        }
        ((TopUpAct) mContext).setNextPage();
    }

    @OnClick(R.id.btn_cancel)
    void cancelClicked() {
        ((TopUpAct) mContext).setPreviousPage();
    }

    @OnClick(R.id.btn_close)
    void closeClicked() {
        ((TopUpAct) mContext).finish();
    }
}

