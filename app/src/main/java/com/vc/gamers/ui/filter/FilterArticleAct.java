package com.vc.gamers.ui.filter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.adapter.TextSelectedAdapter;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.listener.OnFilterSelectedListener;
import com.vc.gamers.model.response.product.ModelText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2020-01-27.
 */

public class FilterArticleAct extends BaseActivity implements OnFilterSelectedListener {

    public static final String TYPE_ALL = "1";
    public static final String TYPE_NEWS = "2";
    public static final String TYPE_PROMO = "3";

    public static final String TYPE_ARTICLE = "typeArticle";

    @BindView(R.id.rv_content_type)
    RecyclerView rvContentType;

    @BindView(R.id.lin_categories)
    LinearLayout linCategories;
    @BindView(R.id.lin_genre)
    LinearLayout linGenre;

    private TextSelectedAdapter typeAdapter;
    private List<ModelText> typeList = new ArrayList<>();

    private String idProductType = TYPE_ALL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);

        linCategories.setVisibility(View.GONE);
        linGenre.setVisibility(View.GONE);

        String idType = getIntent().getStringExtra(TYPE_ARTICLE);
        if (idType != null){
            idProductType = idType;
        }

        initAdapter();
        initData();
    }

    private void initData() {

        typeList.clear();
        typeList.add(new ModelText(TYPE_ALL, "All", 0));
        typeList.add(new ModelText(TYPE_NEWS, "News", 0));
        typeList.add(new ModelText(TYPE_PROMO, "Promo", 0));

        if (idProductType != null) {
            for (ModelText modelText : typeList) {
                if (modelText.getId().equalsIgnoreCase(idProductType)){
                    modelText.setSelected(true);
                    break;
                }
            }
        }
        typeAdapter.replaceItem(typeList);
    }

    private void initAdapter() {
        typeAdapter = new TextSelectedAdapter(this, typeList);
        rvContentType.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvContentType.setAdapter(typeAdapter);
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_apply)
    void applyClicked() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("type", idProductType);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @OnClick(R.id.btn_close)
    void closeClicked() {
        onBackPressed();
    }

    /***** Implement Method *****/
    @Override
    public void onItemFilterSelected(int type, int position) {
        idProductType = typeList.get(position).getId() + "";
    }
}
