package com.vc.gamers.ui.gamesvouchers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.vc.gamers.R;
import com.vc.gamers.adapter.GamesVouchersAdapter;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.presenter.ProductPresenter;
import com.vc.gamers.ui.filter.FilterProductAct;
import com.vc.gamers.utils.RxEdittext;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;

import static android.app.Activity.RESULT_OK;
import static com.vc.gamers.ui.filter.FilterProductAct.CATEGORY_PRODUCT;
import static com.vc.gamers.ui.filter.FilterProductAct.GENRE_PRODUCT;
import static com.vc.gamers.ui.filter.FilterProductAct.TYPE_PRODUCT;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class GamesVouchersFragment extends BaseFragment {

    public static final int FILTER_PRODUCT_CODE = 200;

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.et_search_games_vouchers)
    EditText etSearchGamesVouchers;

    private GamesVouchersAdapter gamesVouchersAdapter;
    private List<Product> gamesVouchersList = new ArrayList<>();
    private Context mContext;
    private String query = "";

    private String type;
    private String genre;
    private String category;

    public static GamesVouchersFragment newInstance() {
        Bundle args = new Bundle();
        GamesVouchersFragment fragment = new GamesVouchersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_games_vouchers, null);
        ButterKnife.bind(this, view);

        initAdapter();

//        getProductSearch("dota2");
        getProductList();
        initEditTextListener();
        return view;
    }

    private void initEditTextListener() {
        etSearchGamesVouchers.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                query = etSearchGamesVouchers.getText().toString();
                if (query.isEmpty()) {
                    getProductList();
                } else {
                    getProductSearch(query);
                }
                hideSoftKeyboard();
                return true;
            }
            return false;
        });
    }

    private void initEditText() {
        RxEdittext.bind(etSearchGamesVouchers)
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter(s -> s.length() >= 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::search);
    }

    private void search(String query) {
        this.query = query;

        if (query.equals("")) {
            getProductList();
            return;
        }

        // TODO : call API from query
        getProductSearch(query);
    }

    private void initAdapter() {
        gamesVouchersAdapter = new GamesVouchersAdapter(mContext, gamesVouchersList);
        rvContent.setLayoutManager(new GridLayoutManager(mContext, 3));
        rvContent.setAdapter(gamesVouchersAdapter);

        swipeRefresh.setOnRefreshListener(() -> {
            if (query.isEmpty()) {
                if (type == null && category == null && genre == null) {
                    getProductList();
                    return;
                }
                if (type == null) {
                    type = "";
                }
                if (category == null) {
                    category = "";
                }
                if (genre == null) {
                    genre = "";
                }
                getProductList(type, category, genre);
            } else {
                getProductSearch(query);
            }

//            if (type != null) {
//                if (query.isEmpty()) {
//                    // TODO : CALL API FILTER
//                    if (type.isEmpty()) {
//                        getProductList();
//                    } else {
//                        getProductList(type);
//                    }
//                } else {
//                    // TODO : CALL API SEARCH
//                    getProductSearch(query);
//                }
//            } else {
//                if (query.isEmpty()) {
//                    getProductList();
//                } else {
//                    getProductSearch(query);
//                }
//            }
        });
    }

    public void refreshData() {
        etSearchGamesVouchers.setText("");
        type = "";
        category = "";
        genre = "";
        getProductList();
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_filter)
    void filterClicked() {
        startActivityForResult(new Intent(mContext, FilterProductAct.class)
                .putExtra(TYPE_PRODUCT, type)
                .putExtra(CATEGORY_PRODUCT, category)
                .putExtra(GENRE_PRODUCT, genre), FILTER_PRODUCT_CODE);
    }

    /***** API Connection *****/
    private void getProductSearch(String name) {
        swipeRefresh.setRefreshing(true);
        ProductPresenter.getProductSearch(name)
                .doOnTerminate(() -> getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    gamesVouchersList.clear();
                    gamesVouchersList.addAll(response.getData());
                    gamesVouchersAdapter.replaceItem(gamesVouchersList);
                }), Throwable::printStackTrace);
    }

    private void getProductList() {
        swipeRefresh.setRefreshing(true);
        ProductPresenter.getProductListAll("0")
                .doOnTerminate(() -> getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    gamesVouchersList.clear();
                    gamesVouchersList.addAll(response.getData());
                    gamesVouchersAdapter.replaceItem(gamesVouchersList);
                }), Throwable::printStackTrace);
    }

    private void getProductList(String type, String categoriId, String genreId) {
        swipeRefresh.setRefreshing(true);
        ProductPresenter.getProductList("0", type, categoriId, genreId)
                .doOnTerminate(() -> getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    gamesVouchersList.clear();
                    gamesVouchersList.addAll(response.getData());
                    gamesVouchersAdapter.replaceItem(gamesVouchersList);
                }), Throwable::printStackTrace);
    }

    /***** Result Handler *****/
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILTER_PRODUCT_CODE) {
            if (resultCode == RESULT_OK) {
                type = data.getStringExtra("type");
                category = data.getStringExtra("category");
                genre = data.getStringExtra("genre");

                if (type == null && category == null && genre == null) {
                    getProductList();
                    return;
                }
                if (type == null) {
                    type = "";
                }
                if (category == null) {
                    category = "";
                }
                if (genre == null) {
                    genre = "";
                }
                getProductList(type, category, genre);
            }
        }
    }
}
