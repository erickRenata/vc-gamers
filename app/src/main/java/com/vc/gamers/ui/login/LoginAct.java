package com.vc.gamers.ui.login;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.persistence.Preferences;
import com.vc.gamers.presenter.UserPresenter;
import com.vc.gamers.ui.dashboard.DashboardAct;
import com.vc.gamers.ui.forgotpass.ForgotPasswordAct;
import com.vc.gamers.ui.register.RegisterAct;
import com.vc.gamers.utils.handler.RxUtils;

import org.json.JSONException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginAct extends BaseActivity {

    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_show_hide_password)
    ImageView ivShowHidePassword;

    @BindView(R.id.login_button_facebook)
    LoginButton btnLoginFb;
    CallbackManager callbackManager;

    String emailFB;
    String nameFB;
    String imageUrlFB;
    private boolean isShowPassword;
    private String tokenFB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

//        etUsername.setText("devel@mail.com");
//        etPassword.setText("devel123");

        if (Preferences.getProfileResponse() != null) {
            startActivity(new Intent(this, DashboardAct.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            overridePendingTransition(0, 0);
        }

        initFacebook();
        initKeyhash();
    }

    private boolean validationForm() {
        if (etUsername.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_username_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_password_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_login)
    void loginClicked() {
        hideSoftKeyboard();
        if (validationForm()) {
            postLogin();
//            initKeyhash();
        }
    }

    @OnClick(R.id.btn_forgot_password)
    void fogotPasswordClicked() {
        startActivity(new Intent(this, ForgotPasswordAct.class));
    }

    @OnClick(R.id.btn_login_facebook)
    void loginFacebookClicked() {
        btnLoginFb.performClick();
    }

    @OnClick(R.id.btn_register_now)
    void registerNowClicked() {
        startActivity(new Intent(this, RegisterAct.class));
    }

    @OnClick(R.id.btn_show_hide_password)
    void showHidePassword() {
        if (isShowPassword){
            isShowPassword = false;
            etPassword.setTransformationMethod(new PasswordTransformationMethod());
            ivShowHidePassword.setImageResource(R.drawable.ic_eye_show);
        }else {
            isShowPassword = true;
            etPassword.setTransformationMethod(null);
            ivShowHidePassword.setImageResource(R.drawable.ic_eye_hide);
        }
    }

    /***** API Connection *****/
    private void postLogin() {
        showLoading();
        UserPresenter.postLogin(etUsername.getText().toString(), etPassword.getText().toString())
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    Preferences.saveLoginResponse(response);
                    getProfile();
                }), Throwable::printStackTrace);
    }

    private void postLoginFacebook(String email, String provider,
                                   String authToken, String idToken,
                                   String photoUrl, String name) {
        showLoading();
        UserPresenter.postLoginFacebook(email, provider, authToken, idToken, photoUrl, name)
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    Preferences.saveLoginResponse(response);
                    getProfile();
                }), Throwable::printStackTrace);
    }

    private void getProfile() {
        UserPresenter.getProfile()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    Preferences.saveProfileRespose(response.getData());
                    startActivity(new Intent(this, DashboardAct.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    finish();
//                    getProfile();
                }), Throwable::printStackTrace);
    }

    /***** Login Facebook *****/
    private void initFacebook() {
        // Facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        // set permission
        btnLoginFb.setReadPermissions(Arrays.asList(
                "public_profile", "email"));//, "user_birthday", "user_friends"));
        // Callback registration
        btnLoginFb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {
                            Log.v("LoginActivity", response.toString());

                            // Application code
                            try {

                                String token = "";
                                String userId = object.getString("id");
//                                String facebookId = object.getString("com");
                                String googleId = "";
//                                String deviceId = CommonUtils.getDeviceId(getApplicationContext());
                                String source = "android";
                                emailFB = object.getString("email");
                                nameFB = object.getString("name");
//                                String birthday = object.getString("birthday");
//                                String lastName = object.getString("last_name");
                                imageUrlFB = "http://graph.facebook.com/" + userId + "/picture?type=large";


                                postLoginFacebook(emailFB, "facebook", tokenFB, tokenFB, imageUrlFB, nameFB);
                                // TODO login by Facebook
                                LoginManager.getInstance().logOut();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                return;
                            }

                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender");//,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
//                Log.v("LoginActivity", exception.getCause().toString());
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        AccessToken token = AccessToken.getCurrentAccessToken();
        if (token != null) {
//            Toast.makeText(this, token.getToken(), Toast.LENGTH_SHORT).show();
            tokenFB = token.getToken();
            LoginManager.getInstance().logOut();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initKeyhash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.vc.gamers",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", keyhash);
            }
        } catch (PackageManager.NameNotFoundException ignored) {

        } catch (NoSuchAlgorithmException ignored) {

        }
    }
}
