package com.vc.gamers.ui.search;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.vc.gamers.R;
import com.vc.gamers.adapter.GamesVouchersAdapter;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.presenter.ProductPresenter;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2020-02-06.
 */

public class SearchAct extends BaseActivity {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.et_search_games_vouchers)
    EditText etSearchGamesVouchers;

    private GamesVouchersAdapter gamesVouchersAdapter;
    private List<Product> gamesVouchersList = new ArrayList<>();

    private String query = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        initAdapter();
        initEditTextListener();

        query = getIntent().getStringExtra("query");
        getProductSearch(query);
        etSearchGamesVouchers.setText(query);
    }

    private void initAdapter() {
        gamesVouchersAdapter = new GamesVouchersAdapter(this, gamesVouchersList);
        rvContent.setLayoutManager(new GridLayoutManager(this, 3));
        rvContent.setAdapter(gamesVouchersAdapter);

        swipeRefresh.setOnRefreshListener(() -> {
            if (query.isEmpty()) {
                getProductSearch("");
            } else {
                getProductSearch(query);
            }
        });
    }

    private void initEditTextListener() {
        etSearchGamesVouchers.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                query = etSearchGamesVouchers.getText().toString();
                if (query.isEmpty()) {
                    getProductSearch("");
                } else {
                    getProductSearch(query);
                }
                hideSoftKeyboard();
                return true;
            }
            return false;
        });
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_back)
    void back() {
        finish();
    }

    /***** API Connection *****/
    private void getProductSearch(String name) {
        swipeRefresh.setRefreshing(true);
        ProductPresenter.getProductSearch(name)
                .doOnTerminate(() -> runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    gamesVouchersList.clear();
                    gamesVouchersList.addAll(response.getData());
                    gamesVouchersAdapter.replaceItem(gamesVouchersList);
                }), Throwable::printStackTrace);
    }
}
