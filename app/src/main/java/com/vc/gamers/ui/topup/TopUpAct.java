package com.vc.gamers.ui.topup;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.model.response.payment.Payment;
import com.vc.gamers.presenter.PaymentMethodPresenter;
import com.vc.gamers.ui.checkout.CheckoutAct;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_DENOMINATION;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_IS_VOUCHER;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_PAYMENT_METHOD;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_PRODUCT;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_USERNAME;

/**
 * Created by @erickrenata on 2019-12-09.
 */

public class TopUpAct extends BaseActivity {

    @BindView(R.id.view_pager_topup)
    ViewPager vPagerTopUp;

    TopUpFragmentStep1 topUpFragmentStep1 = TopUpFragmentStep1.newInstance();
    TopUpFragmentStep2 topUpFragmentStep2 = TopUpFragmentStep2.newInstance();
    TopUpFragmentStep3 topUpFragmentStep3 = TopUpFragmentStep3.newInstance();

    Product product;
    ArrayList<Denomination> denominationList = new ArrayList<>();

    private List<Payment> paymentList = new ArrayList<>();

    int lengthScreen = 3;
    public boolean isVoucher = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);
        ButterKnife.bind(this);

        product = getIntent().getParcelableExtra(Product.class.getSimpleName());
        denominationList = getIntent().getParcelableArrayListExtra(Denomination.class.getSimpleName());

        if (product.getProduct_type_id() == 2) {
            isVoucher = true;
            lengthScreen = 2;
        }
        initViewPager();
        getPaymentMethodList();
    }

    private void initViewPager() {
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getBaseFragmentManager());
        vPagerTopUp.setAdapter(mAdapter);
        vPagerTopUp.setOffscreenPageLimit(lengthScreen);

        if (isVoucher) {
            topUpFragmentStep2.setDenominationList(denominationList);
        }
    }

    public void setNextPage() {
        if (vPagerTopUp.getCurrentItem() == lengthScreen - 3) {
            if (topUpFragmentStep1.etUserId.getText().toString().isEmpty()) {
                Toast.makeText(this, "User ID is required", Toast.LENGTH_SHORT).show();
                return;
            }
            topUpFragmentStep2.setDenominationList(denominationList);
        } else if (vPagerTopUp.getCurrentItem() == lengthScreen - 2) {
            if (topUpFragmentStep2.denominationSelected == null) {
                Toast.makeText(this, "Please choose denomination", Toast.LENGTH_SHORT).show();
                return;
            }

            topUpFragmentStep3.initData(paymentList, topUpFragmentStep2.denominationSelected);
        } else if (vPagerTopUp.getCurrentItem() == lengthScreen - 1) {
            startActivity(new Intent(this, CheckoutAct.class)
                    .putExtra(CHECKOUT_PAYMENT_METHOD, topUpFragmentStep3.paymentMethod)
                    .putExtra(CHECKOUT_PRODUCT, product)
                    .putExtra(CHECKOUT_USERNAME, isVoucher ? "" : topUpFragmentStep1.etUserId.getText().toString())
                    .putExtra(CHECKOUT_IS_VOUCHER, isVoucher)
                    .putExtra(CHECKOUT_DENOMINATION, topUpFragmentStep2.denominationSelected));
            return;
        }
        vPagerTopUp.setCurrentItem(vPagerTopUp.getCurrentItem() + 1);
    }

    public void setPreviousPage() {
        if (vPagerTopUp.getCurrentItem() == 0) {
            finish();
            return;
        }
        vPagerTopUp.setCurrentItem(vPagerTopUp.getCurrentItem() - 1);
    }

    /***** Inner Class Adapter *****/
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                if (!isVoucher) {
                    return topUpFragmentStep1;
                } else {
                    return topUpFragmentStep2;
                }
            } else if (position == 1) {
                if (!isVoucher) {
                    return topUpFragmentStep2;
                } else {
                    return topUpFragmentStep3;
                }
            } else if (position == 2) {
                return topUpFragmentStep3;
            }
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }

        @Override
        public int getCount() {
            return lengthScreen;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    /***** API Connection *****/
    private void getPaymentMethodList() {
        showLoading();
        PaymentMethodPresenter.getPaymentMethodList()
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    paymentList.clear();
                    paymentList.addAll(response.getData());
                }), Throwable::printStackTrace);
    }
}

