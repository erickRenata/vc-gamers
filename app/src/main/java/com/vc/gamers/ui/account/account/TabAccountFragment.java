package com.vc.gamers.ui.account.account;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.profile.Profile;
import com.vc.gamers.persistence.Preferences;
import com.vc.gamers.presenter.UserPresenter;
import com.vc.gamers.ui.dashboard.DashboardAct;
import com.vc.gamers.ui.login.LoginAct;
import com.vc.gamers.utils.Utils;
import com.vc.gamers.utils.handler.RxUtils;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by @erickrenata on 2019-12-13.
 */

public class TabAccountFragment extends BaseFragment {

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_GALLERY = 200;

    @BindView(R.id.iv_profile)
    public ImageView ivProfile;
    @BindView(R.id.tv_name)
    EditText etName;
    @BindView(R.id.tv_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.tv_birth_date)
    EditText etBirthDate;

    @BindView(R.id.et_current_pass)
    EditText etCurrentPass;
    @BindView(R.id.et_new_pass)
    EditText etNewPass;
    @BindView(R.id.et_confirm_pass)
    EditText etConfirmPass;

    private Context mContext;

    public int selectedHolderImage;
    public Uri file;
    private int userYear;
    private int userMonth;
    private int userDay;

    public static TabAccountFragment newInstance() {
        Bundle args = new Bundle();
        TabAccountFragment fragment = new TabAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_account, null);
        ButterKnife.bind(this, view);

        initData();
        return view;
    }

    private void initData() {
        Profile profile = Preferences.getProfileResponse();
        if (profile.getImage() != null) {
            if (!profile.getImage().isEmpty()) {
                Picasso.get().load(profile.getImage()).into(ivProfile);
            }
        }
        etName.setText(profile.getName());
        etPhoneNumber.setText(profile.getPhone());
        if (profile.getBirth_date() != null) {
            etBirthDate.setText(Utils.formatDateMappingProfile(profile.getBirth_date()));
        }

        final Calendar myCalendar = Calendar.getInstance();
        userYear = myCalendar.get(Calendar.YEAR);
        userMonth = myCalendar.get(Calendar.MONTH);
        userDay = myCalendar.get(Calendar.DAY_OF_MONTH);
        if (profile.getBirth_date() != null) {
            if (!profile.getBirth_date().isEmpty()) {
                userYear = Integer.parseInt(Utils.getSpecificTime(profile.getBirth_date(), "year"));
                userMonth = Integer.parseInt(Utils.getSpecificTime(profile.getBirth_date(), "month")) - 1;
                userDay = Integer.parseInt(Utils.getSpecificTime(profile.getBirth_date(), "day"));
            }
        }
    }

    private boolean validationFormProfile() {
        if (etName.getText().toString().isEmpty()) {
            Toast.makeText(mContext, getString(R.string.error_name_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etPhoneNumber.getText().toString().trim().isEmpty()) {
            Toast.makeText(mContext, getString(R.string.error_phone_number_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etBirthDate.getText().toString().isEmpty()) {
            Toast.makeText(mContext, getString(R.string.error_birth_date_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validationFormPassword() {
        if (etCurrentPass.getText().toString().isEmpty()) {
            Toast.makeText(mContext, getString(R.string.error_current_password_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etNewPass.getText().toString().isEmpty()) {
            Toast.makeText(mContext, getString(R.string.error_new_password_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etConfirmPass.getText().toString().isEmpty()) {
            Toast.makeText(mContext, getString(R.string.error_confirmation_password_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!etNewPass.getText().toString().equalsIgnoreCase(etConfirmPass.getText().toString())) {
            Toast.makeText(mContext, getString(R.string.error_missmatch_password_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_logout)
    void logout() {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    startActivity(new Intent(mContext, DashboardAct.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                    // TODO : remove all memory
                    Preferences.removeLoginResponse();
                    Preferences.removeProfileResponse();
                    Preferences.removeRememberMe();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.dismiss();
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure want to sign out?")
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @OnClick(R.id.btn_save)
    void saveUpdateProfile() {
        hideSoftKeyboard();
        showLoading();

        if (!etCurrentPass.getText().toString().isEmpty()) {
            if (validationFormPassword()) {
                postUpdatePassword();
            }
        }
        if (!etName.getText().toString().isEmpty() && !etBirthDate.getText().toString().isEmpty()
                && !etPhoneNumber.getText().toString().trim().isEmpty()) {
            if (validationFormProfile()) {
                postUpdateProfile(file, etName.getText().toString(), Preferences.getProfileResponse().getEmail(), etPhoneNumber.getText().toString().trim(), etBirthDate.getText().toString());
            }
        }
    }

    @OnClick(R.id.btn_change_photo)
    void changePhoto() {
//        String[] colors = {"From Camera", "From Gallery"};
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//        builder.setTitle("Change Photo");
//        builder.setItems(colors, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                // the user clicked on colors[which]
//                if (which == 0) {
                    requestPermissions(
                            new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.ACCESS_NETWORK_STATE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE},
                            PICK_FROM_CAMERA);
//                } else {
//                    requestPermissions(
//                            new String[]{
//                                    Manifest.permission.CAMERA,
//                                    Manifest.permission.ACCESS_NETWORK_STATE,
//                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                                    Manifest.permission.READ_EXTERNAL_STORAGE},
//                            PICK_FROM_GALLERY);
//                }
//            }
//        });
//        builder.show();
    }

    @OnClick(R.id.btn_birthdate)
    void showDatePicker() {
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, (datePicker, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            etBirthDate.setText(Utils.formatDateMappingProfile(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));

            userYear = year;
            userMonth = monthOfYear;
            userDay = dayOfMonth;
        }, userYear, userMonth, userDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    /***** API Connection *****/
    public void postUpdateProfile(Uri uri, String name, String email, String phoneNumber, String birthDate) {
        UserPresenter.postUpdateProfile(uri != null ? new File(Utils.getRealPathFromURI(uri, mContext)) : null, name, email, phoneNumber, Utils.formatDateForUpdateParam(birthDate))
                .doOnTerminate(() -> getActivity().runOnUiThread(() -> dismissLoading()))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    getProfile();
                    Toast.makeText(mContext, "Profile Updated", Toast.LENGTH_SHORT).show();
                }), Throwable::printStackTrace);
    }

    private void getProfile() {
        UserPresenter.getProfile()
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    Preferences.saveProfileRespose(response.getData());
//                    initData();
                }), Throwable::printStackTrace);
    }

    private void postUpdatePassword() {
        UserPresenter.postUpdatePassword(etCurrentPass.getText().toString(), etNewPass.getText().toString(), etConfirmPass.getText().toString())
                .doOnTerminate(() -> getActivity().runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    etCurrentPass.setText("");
                    etNewPass.setText("");
                    etConfirmPass.setText("");

                    Toast.makeText(mContext, response.getMessage(), Toast.LENGTH_SHORT).show();
                }), Throwable::printStackTrace);
    }

    /***** Result *****/
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PICK_FROM_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(getActivity());

//                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//                    StrictMode.setVmPolicy(builder.build());
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    file = Uri.fromFile(Utils.getOutputMediaFile());
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
//                    startActivityForResult(intent, PICK_FROM_CAMERA);
                } else {
                    Toast.makeText(mContext, "Permission denied. Cannot open camera.", Toast.LENGTH_SHORT).show();
                }
                break;
            case PICK_FROM_GALLERY:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, PICK_FROM_GALLERY);
                } else {
                    Toast.makeText(mContext, "Permission denied. Cannot choose an image.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                file = resultUri;
                ivProfile.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(mContext, error.toString(), Toast.LENGTH_SHORT).show();
            }
        } else {
            switch (requestCode) {
                case PICK_FROM_CAMERA:
                    if (resultCode == RESULT_OK) {
//                    accountFragment.tabAccountFragment.ivProfile.setImageURI(accountFragment.tabAccountFragment.file);
                        ivProfile.setImageURI(file);
                    }
                    break;
                case PICK_FROM_GALLERY:
                    if (resultCode == RESULT_OK) {
                        file = data.getData();
                        ivProfile.setImageURI(file);
                    }
                    break;
            }
        }
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        getProfile();
//    }
}
