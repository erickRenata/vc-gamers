package com.vc.gamers.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.santalu.autoviewpager.AutoViewPager;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.vc.gamers.R;
import com.vc.gamers.adapter.GamesVouchersAdapter;
import com.vc.gamers.adapter.TopGamesAdapter;
import com.vc.gamers.adapter.VouchersAdapter;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.banner.Banner;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.presenter.BannerPresenter;
import com.vc.gamers.presenter.ProductPresenter;
import com.vc.gamers.ui.banner.BannerFragment;
import com.vc.gamers.ui.search.SearchAct;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.vc.gamers.utils.Utils.convertToPx;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class HomeFragment extends BaseFragment {

    @BindView(R.id.vp_banner)
    AutoViewPager vpBanner;
    @BindView(R.id.dots_indicator)
    DotsIndicator dotsIndicator;

    @BindView(R.id.rv_content_top_games)
    RecyclerView rvContentTopGames;
    @BindView(R.id.rv_content_flash_topup)
    RecyclerView rvContentFlashTopup;
    @BindView(R.id.rv_content_vouchers)
    RecyclerView rvContentVouchers;

    @BindView(R.id.et_search_games_vouchers)
    EditText etSearch;

    private TopGamesAdapter topGamesAdapter;
    private List<Product> topGamesList = new ArrayList<>();
    private GamesVouchersAdapter gamesVouchersAdapter;
    private List<Product> flashTopUpList = new ArrayList<>();
    private VouchersAdapter vouchersAdapter;
    private List<Product> voucherList = new ArrayList<>();

    private Context mContext;

    private List<BannerFragment> bannerFragmentList = new ArrayList<>();

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null);
        ButterKnife.bind(this, view);

        initEditTextListener();
        initAdapter();

        getBannerList();
        getProductTopList();
        getProductFlashTopupList();
        getProductVoucherList();
        return view;
    }

    private void initEditTextListener() {
        etSearch.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                hideSoftKeyboard();
                if (!etSearch.getText().toString().isEmpty()){
                    startActivity(new Intent(mContext, SearchAct.class)
                            .putExtra("query", etSearch.getText().toString()));

                    etSearch.setText("");
                }
                return true;
            }
            return false;
        });
    }

    private void initBanner() {
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getChildFragmentManager());
        vpBanner.setAdapter(mAdapter);
        vpBanner.setClipToPadding(false);
        vpBanner.setPadding(convertToPx(16), 0, convertToPx(40), 0);
        vpBanner.setPageMargin(convertToPx(16));
        vpBanner.setOffscreenPageLimit(bannerFragmentList.size());
        vpBanner.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                if (position == 0) {
//                    vpBanner.setPadding(convertToPx(16), 0, convertToPx(40), 0);
//                } else
                if (position == bannerFragmentList.size() - 1) {
                    vpBanner.setPadding(convertToPx(40), 0, convertToPx(16), 0);

                } else {
//                    vpBanner.setPadding(convertToPx(28), 0, convertToPx(28), 0);
                    vpBanner.setPadding(convertToPx(16), 0, convertToPx(40), 0);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        dotsIndicator.setViewPager(vpBanner);
    }

    private void initAdapter() {
        topGamesAdapter = new TopGamesAdapter(mContext, topGamesList);
        rvContentTopGames.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        rvContentTopGames.setAdapter(topGamesAdapter);

        gamesVouchersAdapter = new GamesVouchersAdapter(mContext, flashTopUpList);
        rvContentFlashTopup.setLayoutManager(new GridLayoutManager(mContext, 3));
        rvContentFlashTopup.setAdapter(gamesVouchersAdapter);

        vouchersAdapter = new VouchersAdapter(mContext, voucherList);
        rvContentVouchers.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        rvContentVouchers.setAdapter(vouchersAdapter);
    }

    /***** Inner Class Adapter *****/
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return bannerFragmentList.get(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }

        @Override
        public int getCount() {
            return bannerFragmentList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    /***** API Connection *****/
    private void getBannerList() {
//        showLoading();
        BannerPresenter.getBannerList()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    bannerFragmentList.clear();
                    for (Banner banner : response.getData()) {
                        bannerFragmentList.add(BannerFragment.newInstance(banner));
                    }
                    initBanner();
                }), Throwable::printStackTrace);
    }

    private void getProductTopList() {
//        showLoading();
        ProductPresenter.getProductTopList()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    topGamesList.clear();
                    topGamesList.addAll(response.getData());
                    topGamesAdapter.replaceItem(topGamesList);
                }), Throwable::printStackTrace);
    }

    private void getProductFlashTopupList() {
//        showLoading();
        ProductPresenter.getProductList("0", "1", "", "")
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    flashTopUpList.clear();
                    flashTopUpList.addAll(response.getData());
                    gamesVouchersAdapter.replaceItem(flashTopUpList);
                }), Throwable::printStackTrace);
    }

    private void getProductVoucherList() {
//        showLoading();
        ProductPresenter.getProductList("0", "2", "", "")
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    voucherList.clear();
                    voucherList.addAll(response.getData());
                    vouchersAdapter.replaceItem(voucherList);
                }), Throwable::printStackTrace);
    }
}
