package com.vc.gamers.ui.filter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.adapter.TextSelectedAdapter;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.listener.OnFilterSelectedListener;
import com.vc.gamers.model.response.product.Category;
import com.vc.gamers.model.response.product.Genre;
import com.vc.gamers.model.response.product.ModelText;
import com.vc.gamers.model.response.product.Type;
import com.vc.gamers.persistence.Preferences;
import com.vc.gamers.presenter.ProductPresenter;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.vc.gamers.adapter.TextSelectedAdapter.PRODUCT_CATEGORY;
import static com.vc.gamers.adapter.TextSelectedAdapter.PRODUCT_TYPE;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class FilterProductAct extends BaseActivity implements OnFilterSelectedListener {

    public static final String TYPE_PRODUCT = "typeProduct";
    public static final String CATEGORY_PRODUCT = "categoryProduct";
    public static final String GENRE_PRODUCT = "genreProduct";

    @BindView(R.id.rv_content_type)
    RecyclerView rvContentType;
    @BindView(R.id.rv_content_categories)
    RecyclerView rvContentCategories;

    @BindView(R.id.spin_username)
    Spinner spinGenre;

    private TextSelectedAdapter typeAdapter;
    private List<ModelText> typeList = new ArrayList<>();

    private TextSelectedAdapter categoriesAdapter;
    private List<ModelText> categoryList = new ArrayList<>();

    private List<ModelText> genreList = new ArrayList<>();
    private String[] arrGenre = new String[]{};

    private String idProductType = "";
    private String idProductCategory = "";
    private String idProductGenre = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);

        String idType = getIntent().getStringExtra(TYPE_PRODUCT);
        if (idType != null){
            idProductType = idType;
        }
        String idCategory = getIntent().getStringExtra(CATEGORY_PRODUCT);
        if (idType != null){
            idProductCategory = idCategory;
        }
        String idGenre = getIntent().getStringExtra(GENRE_PRODUCT);
        if (idType != null){
            idProductGenre = idGenre;
        }

        initAdapter();
        initSpinner();

        initData();
    }

    private void initAPI() {
        getProductCategory();
        getProductGenre();
        getProductType();
    }

    private void initData() {
        if (Preferences.getFilterType() == null) {
            initAPI();
            return;
        }
        typeList.clear();
        typeList.addAll(Preferences.getFilterType());
        if (idProductType != null) {
            for (ModelText modelText : typeList) {
                if (modelText.getId().equalsIgnoreCase(idProductType)){
                    modelText.setSelected(true);
                    break;
                }
            }
        }
        typeAdapter.replaceItem(typeList);

        if (Preferences.getFilterCategory() == null) {
            initAPI();
            return;
        }
        categoryList.clear();
        categoryList.addAll(Preferences.getFilterCategory());
        if (idProductCategory != null) {
            for (ModelText modelText : categoryList) {
                if (modelText.getId().equalsIgnoreCase(idProductCategory)){
                    modelText.setSelected(true);
                    break;
                }
            }
        }
        categoriesAdapter.replaceItem(categoryList);

        if (Preferences.getFilterGenre() == null) {
            initAPI();
            return;
        }
        genreList.clear();
        genreList.addAll(Preferences.getFilterGenre());
        initSpinner();
    }

    private void initAdapter() {
        typeAdapter = new TextSelectedAdapter(this, typeList);
        rvContentType.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvContentType.setAdapter(typeAdapter);

        categoriesAdapter = new TextSelectedAdapter(this, categoryList);
        rvContentCategories.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvContentCategories.setAdapter(categoriesAdapter);
    }

    private void initSpinner() {
        arrGenre = new String[genreList.size()];
        for (int i = 0; i < genreList.size(); i++) {
            arrGenre[i] = genreList.get(i).getName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arrGenre);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinGenre.setAdapter(adapter);

        if (idProductGenre != null) {
            for (int i = 0; i < genreList.size(); i++) {
                if (genreList.get(i).getId().equalsIgnoreCase(idProductGenre)){
                    spinGenre.setSelection(i);
                    break;
                }
            }
        }
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_apply)
    void applyClicked() {
        if (spinGenre.getSelectedItemPosition() != 0) {
            idProductGenre = genreList.get(spinGenre.getSelectedItemPosition()).getId() + "";
        } else {
            idProductGenre = "";
        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("type", idProductType);
        returnIntent.putExtra("category", idProductCategory);
        returnIntent.putExtra("genre", idProductGenre);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @OnClick(R.id.btn_close)
    void closeClicked() {
        onBackPressed();
    }

    /***** API Connection *****/
    private void getProductType() {
        showLoading();
        ProductPresenter.getProductType()
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    typeList.clear();
                    typeList.add(new ModelText("", "All Type", PRODUCT_TYPE));
                    for (Type type : response.getData()) {
                        typeList.add(new ModelText(type.getId() + "", type.getType(), PRODUCT_TYPE));
                    }
                    Preferences.saveFilterType(typeList);

                    if (idProductType != null) {
                        for (ModelText modelText : typeList) {
                            if (modelText.getId().equalsIgnoreCase(idProductType)){
                                modelText.setSelected(true);
                                break;
                            }
                        }
                    }
                    typeAdapter.replaceItem(typeList);
                }), Throwable::printStackTrace);
    }

    private void getProductCategory() {
        ProductPresenter.getProductCategory()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    categoryList.clear();

                    categoryList.add(new ModelText("", "All Categories", PRODUCT_CATEGORY));
                    for (Category category : response.getData()) {
                        categoryList.add(new ModelText(category.getId() + "", category.getName(), PRODUCT_CATEGORY));
                    }
                    Preferences.saveFilterCategory(categoryList);
                    if (idProductCategory != null) {
                        for (ModelText modelText : categoryList) {
                            if (modelText.getId().equalsIgnoreCase(idProductCategory)){
                                modelText.setSelected(true);
                                break;
                            }
                        }
                    }
                    categoriesAdapter.replaceItem(categoryList);
                }), Throwable::printStackTrace);
    }

    private void getProductGenre() {
        ProductPresenter.getProductGenre()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    genreList.clear();

                    genreList.add(new ModelText("", "All Genre", 0));
                    for (Genre genre : response.getData()) {
                        genreList.add(new ModelText(genre.getId() + "", genre.getGenre(), 0));
                    }
                    Preferences.saveFilterGenre(genreList);
                    initSpinner();
                }), Throwable::printStackTrace);
    }

    /***** Implement Method *****/
    @Override
    public void onItemFilterSelected(int type, int position) {
        switch (type) {
            case PRODUCT_TYPE:
                idProductType = typeList.get(position).getId() + "";
                break;
            case PRODUCT_CATEGORY:
                idProductCategory = categoryList.get(position).getId() + "";
                break;
        }
    }
}
