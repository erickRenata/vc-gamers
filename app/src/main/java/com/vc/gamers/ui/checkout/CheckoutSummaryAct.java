package com.vc.gamers.ui.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.model.response.payment.PaymentMethod;
import com.vc.gamers.ui.dashboard.DashboardAct;
import com.vc.gamers.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_ACCOUNT_NUMBER;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_ADDITIONAL;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_CODE;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_EMAIL;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_GRAND_TOTAL;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_ID;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_IS_VOUCHER;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_PAYMENT_METHOD;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_PRODUCT;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_USERNAME;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_VA_NUMBERS;

/**
 * Created by @erickrenata on 2020-02-05.
 */

public class CheckoutSummaryAct extends BaseActivity {

    @BindView(R.id.tv_checkout_using)
    TextView tvCheckoutUsing;
    @BindView(R.id.iv_payment_method)
    ImageView ivPaymentMethod;
    @BindView(R.id.tv_checkout_item)
    TextView tvCheckoutItem;
    @BindView(R.id.tv_checkout_username)
    TextView tvCheckoutUsername;
    @BindView(R.id.tv_checkout_email)
    TextView tvCheckoutEmail;
    @BindView(R.id.tv_checkout_price)
    TextView tvCheckoutPrice;
    @BindView(R.id.tv_checkout_price_total)
    TextView tvCheckoutPriceTotal;
    @BindView(R.id.tv_va_numbers)
    TextView tvVANumbers;
    @BindView(R.id.tv_additional)
    TextView tvAdditional;
    @BindView(R.id.tv_transaction_id)
    TextView tvTransactionId;
    @BindView(R.id.tv_payment_code)
    TextView tvPaymentCode;
    @BindView(R.id.tv_unique_code)
    TextView tvUniqueCode;

    @BindView(R.id.lin_user_id)
    LinearLayout linUserID;
    @BindView(R.id.lin_email)
    LinearLayout linEmail;
    @BindView(R.id.lin_va_numbers)
    LinearLayout linVANumbers;
    @BindView(R.id.lin_additional)
    LinearLayout linAdditional;
    @BindView(R.id.lin_unique_code)
    LinearLayout linUniqueCode;

    Product product;
    PaymentMethod paymentMethod;
    String username;
    String vaNumber;
    String email;
    String additional;
    String transactionId;
    boolean isVoucher;
    String accountNumber;
    int uniqueCode;
    private int grandTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_summary);
        ButterKnife.bind(this);

        product = getIntent().getParcelableExtra(CHECKOUT_PRODUCT);
        paymentMethod = getIntent().getParcelableExtra(CHECKOUT_PAYMENT_METHOD);
        username = getIntent().getStringExtra(CHECKOUT_USERNAME);
        email = getIntent().getStringExtra(CHECKOUT_EMAIL);
        isVoucher = getIntent().getBooleanExtra(CHECKOUT_IS_VOUCHER, false);
        vaNumber = getIntent().getStringExtra(CHECKOUT_VA_NUMBERS);
        grandTotal = getIntent().getIntExtra(CHECKOUT_GRAND_TOTAL, 0);
        accountNumber = getIntent().getStringExtra(CHECKOUT_ACCOUNT_NUMBER);
        uniqueCode = getIntent().getIntExtra(CHECKOUT_CODE, 0);
        additional = getIntent().getStringExtra(CHECKOUT_ADDITIONAL);
        transactionId = getIntent().getStringExtra(CHECKOUT_ID);

        initView();
    }

    private void initView() {
        tvCheckoutUsing.setText("Checkout using " + paymentMethod.getName());
        tvCheckoutItem.setText(product.getName());
        tvCheckoutUsername.setText(username);
        tvCheckoutPrice.setText(Utils.convertNumberWithRp(paymentMethod.getPaymentDenomination().getPrice_item()));
        tvCheckoutPriceTotal.setText(Utils.convertNumberWithRp(grandTotal));
        tvCheckoutEmail.setText(email);
        tvTransactionId.setText(transactionId);
        Picasso.get().load(paymentMethod.getImage()).into(ivPaymentMethod);
        tvPaymentCode.setText(accountNumber);

        linUniqueCode.setVisibility(View.GONE);
        if (uniqueCode > 0) {
            linUniqueCode.setVisibility(View.VISIBLE);
            tvUniqueCode.setText(uniqueCode + "");
        }

        if (isVoucher) {
            linUserID.setVisibility(View.GONE);
            linEmail.setVisibility(View.VISIBLE);
        } else {
            linUserID.setVisibility(View.VISIBLE);
            linEmail.setVisibility(View.GONE);
        }

        linAdditional.setVisibility(View.GONE);
        if (!additional.isEmpty()) {
            linAdditional.setVisibility(View.VISIBLE);
            tvAdditional.setText(additional);
        }

        linVANumbers.setVisibility(View.VISIBLE);
        if (vaNumber != null) {
            if (!vaNumber.isEmpty()) {
                tvVANumbers.setText(vaNumber);
            } else {
                tvVANumbers.setText(accountNumber);
            }
        } else {
            tvVANumbers.setText(accountNumber);
        }
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_confirm)
    void confirmCheckout() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        startActivity(new Intent(this, DashboardAct.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }
}
