package com.vc.gamers.ui.topup;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.vc.gamers.R;
import com.vc.gamers.adapter.DenominationAdapter;
import com.vc.gamers.adapter.PaymentMethodListAdapter;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.listener.OnClickListener;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.game.PaymentDenomination;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.model.response.payment.Payment;
import com.vc.gamers.model.response.payment.PaymentMethod;
import com.vc.gamers.presenter.PaymentMethodPresenter;
import com.vc.gamers.ui.checkout.CheckoutAct;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_ADDITIONAL;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_DENOMINATION;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_IS_VOUCHER;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_PAYMENT_METHOD;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_PRODUCT;
import static com.vc.gamers.ui.checkout.CheckoutAct.CHECKOUT_USERNAME;

/**
 * Created by @erickrenata on 17/03/20.
 */

public class NewTopUpAct extends BaseActivity implements OnClickListener {

    @BindView(R.id.rv_content_denom)
    RecyclerView rvContentDenom;
    @BindView(R.id.rv_content_method)
    RecyclerView rvContentMethod;

    @BindView(R.id.et_user_id)
    EditText etUserId;
    @BindView(R.id.et_additional)
    TextInputEditText etAdditional;
    @BindView(R.id.tl_additional)
    TextInputLayout tlAdditional;

    @BindView(R.id.lin_step_1_content)
    LinearLayout linStep1Content;
    @BindView(R.id.lin_step_2)
    LinearLayout linStep2;
    @BindView(R.id.lin_step_2_voucher)
    LinearLayout linStep2Voucher;
    @BindView(R.id.lin_step_3)
    LinearLayout linStep3;

    private int hasAdditionalField;
    private Product product;
    private ArrayList<Denomination> denominationList = new ArrayList<>();
    private DenominationAdapter denominationAdapter;
    private Denomination denominationSelected;

    private PaymentMethodListAdapter topUpPaymentAdapter;
    private List<Payment> paymentList = new ArrayList<>();
    private boolean isVoucher = false;
    private List<Payment> paymentListMethod = new ArrayList<>();
    public PaymentMethod paymentMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_topup);
        ButterKnife.bind(this);

        hasAdditionalField = getIntent().getIntExtra("additional", 0);
        product = getIntent().getParcelableExtra(Product.class.getSimpleName());
        denominationList = getIntent().getParcelableArrayListExtra(Denomination.class.getSimpleName());
        initAdapter();

        if (product.getProduct_type_id() == 2) {
            isVoucher = true;
        }
        getPaymentMethodList();

        initView();
    }

    private void initView() {
        if (isVoucher) {
            linStep1Content.setVisibility(View.GONE);
            linStep2.setVisibility(View.GONE);
            linStep3.setVisibility(View.GONE);
            linStep2Voucher.setVisibility(View.VISIBLE);
        } else {
            if (hasAdditionalField == 1) {
                tlAdditional.setVisibility(View.VISIBLE);
                tlAdditional.setHint("Server Number");
//                tlAdditional.setHint(Html.fromHtml(product.getNote_additional_field()));
            }
        }
    }

    private void initAdapter() {
        denominationAdapter = new DenominationAdapter(this, denominationList, null);
        rvContentDenom.setLayoutManager(new GridLayoutManager(this, 2));
        rvContentDenom.setAdapter(denominationAdapter);
    }

    public void initData(List<Payment> paymentList, Denomination denominationSelected) {
//        this.paymentList = paymentList;
        this.paymentListMethod.clear();
        this.denominationSelected = denominationSelected;

        List<Payment> paymentsTemp = new ArrayList<>();
        for (Payment payment : paymentList) {
            List<PaymentMethod> paymentMethodList = new ArrayList<>();
            for (PaymentDenomination paymentDenomination : denominationSelected.getPayment()) {
                for (PaymentMethod paymentMethod : payment.getMethods()) {
                    if (paymentMethod.getId() == paymentDenomination.getId()) {
                        paymentMethod.setPaymentDenomination(paymentDenomination);
                        paymentMethodList.add(paymentMethod);
                    }
                }
            }
            payment.setMethods(paymentMethodList);
            paymentsTemp.add(payment);
        }
        this.paymentListMethod.addAll(paymentsTemp);

        topUpPaymentAdapter = new PaymentMethodListAdapter(this, paymentListMethod, null, denominationSelected);
        rvContentMethod.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvContentMethod.setAdapter(topUpPaymentAdapter);
    }

    public void onSelectedPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_show_image_userid)
    void showImageUserID() {
        UserIDDialog.newInstance((this).product.getScreenshoot_id()).show(getBaseFragmentManager());
    }

    @OnClick(R.id.btn_next)
    void nextClicked() {
        if (!isVoucher) {
            if (etUserId.getText().toString().isEmpty()) {
                Toast.makeText(this, "User ID is required", Toast.LENGTH_SHORT).show();
                return;
            }
            if (product.getHas_additional_field() == 1) {
                if (etAdditional.getText().toString().isEmpty()) {
                    Toast.makeText(this, Html.fromHtml(product.getNote_additional_field()) + " is required", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }
        if (paymentMethod == null) {
            Toast.makeText(this, "Payment Method Required", Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(new Intent(this, CheckoutAct.class)
                .putExtra(CHECKOUT_PAYMENT_METHOD, paymentMethod)
                .putExtra(CHECKOUT_PRODUCT, product)
                .putExtra(CHECKOUT_USERNAME, isVoucher ? "" : etUserId.getText().toString())
                .putExtra(CHECKOUT_IS_VOUCHER, isVoucher)
                .putExtra(CHECKOUT_DENOMINATION, denominationSelected)
                .putExtra(CHECKOUT_ADDITIONAL, product.getHas_additional_field() == 1 ? etAdditional.getText().toString() : ""));

    }

    @OnClick({R.id.btn_cancel, R.id.btn_close})
    void closeClicked() {
        finish();
    }

    /***** API Connection *****/
    private void getPaymentMethodList() {
        showLoading();
        PaymentMethodPresenter.getPaymentMethodList()
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    paymentList.clear();
                    paymentList.addAll(response.getData());

                    if (denominationList.size() > 0) {
                        onItemClicked(0);
                        denominationList.get(0).setIs_selected(true);
                    }
                    denominationAdapter.replaceItem(denominationList);
                }), Throwable::printStackTrace);
    }

    /***** Implement Method *****/
    @Override
    public void onItemClicked(int position) {
        denominationSelected = denominationList.get(position);
        initData(paymentList, denominationSelected);
    }
}
