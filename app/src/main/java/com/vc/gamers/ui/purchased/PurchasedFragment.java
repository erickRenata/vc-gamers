package com.vc.gamers.ui.purchased;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.vc.gamers.R;
import com.vc.gamers.adapter.PurchasedAdapter;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.history.Order;
import com.vc.gamers.persistence.Preferences;
import com.vc.gamers.presenter.HistoryPresenter;
import com.vc.gamers.ui.login.LoginAct;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class PurchasedFragment extends BaseFragment {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.lin_guest)
    LinearLayout linGuest;
    @BindView(R.id.lin_content)
    LinearLayout linContent;

    private PurchasedAdapter purchasedAdapter;
    private List<Order> purchasedList = new ArrayList<>();
    private Context mContext;

    public static PurchasedFragment newInstance() {
        Bundle args = new Bundle();
        PurchasedFragment fragment = new PurchasedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchased, null);
        ButterKnife.bind(this, view);

        if (Preferences.getLoginResponse() != null) {
            initAdapter();
            getPurchasedOrder();
        } else {
            linContent.setVisibility(View.GONE);
            linGuest.setVisibility(View.VISIBLE);
        }
        return view;
    }

    private void initAdapter() {
        purchasedAdapter = new PurchasedAdapter(mContext, purchasedList);
        rvContent.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        rvContent.setAdapter(purchasedAdapter);
        swipeRefresh.setOnRefreshListener(this::getPurchasedOrder);
    }

    public void refreshData() {
        if (Preferences.getLoginResponse() == null){
            return;
        }
        getPurchasedOrder();
    }

    /***** API Connection *****/
    private void getPurchasedOrder() {
        swipeRefresh.setRefreshing(true);
        HistoryPresenter.getPurchasedOrder()
                .doOnTerminate(() -> getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    purchasedList.clear();
                    purchasedList.addAll(response.getData());
                    purchasedAdapter.replaceItem(purchasedList);
                }), Throwable::printStackTrace);
    }

    @OnClick(R.id.btn_login)
    void navigateToLogin() {
        startActivity(new Intent(mContext, LoginAct.class));
    }
}

