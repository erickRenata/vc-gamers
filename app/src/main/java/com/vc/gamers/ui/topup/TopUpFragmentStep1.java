package com.vc.gamers.ui.topup;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.product.RememberMe;
import com.vc.gamers.persistence.Preferences;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2019-12-10.
 */

public class TopUpFragmentStep1 extends BaseFragment {

    @BindView(R.id.et_user_id)
    public EditText etUserId;

    private Context mContext;

    public static TopUpFragmentStep1 newInstance() {
        Bundle args = new Bundle();
        TopUpFragmentStep1 fragment = new TopUpFragmentStep1();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_up_1, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
    }

    public void initView() {
        if (Preferences.getRememberMe() != null) {
            List<RememberMe> rememberMeList = Preferences.getRememberMe();
            for (RememberMe rememberMe : rememberMeList) {
                // TODO : validate if game id is available
                etUserId.setText(rememberMe.getUserId());
            }
        }
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_show_image_userid)
    void showImageUserID() {
        UserIDDialog.newInstance(((TopUpAct) mContext).product.getScreenshoot_id()).show(getFragmentManager());
    }

    @OnClick(R.id.btn_next)
    void nextClicked() {
        ((TopUpAct) mContext).setNextPage();
    }

    @OnClick(R.id.btn_cancel)
    void cancelClicked() {
        ((TopUpAct) mContext).setPreviousPage();
    }

    @OnClick(R.id.btn_close)
    void closeClicked() {
        ((TopUpAct) mContext).finish();
    }
}
