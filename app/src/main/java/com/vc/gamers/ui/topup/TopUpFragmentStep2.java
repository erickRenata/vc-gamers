package com.vc.gamers.ui.topup;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.adapter.DenominationAdapter;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.listener.OnClickListener;
import com.vc.gamers.model.response.game.Denomination;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2019-12-10.
 */

public class TopUpFragmentStep2 extends BaseFragment implements OnClickListener {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;

    @BindView(R.id.lin_step_1)
    LinearLayout linStep1;
    @BindView(R.id.lin_step_2)
    LinearLayout linStep2;

    private Context mContext;
    private ArrayList<Denomination> denominationList = new ArrayList<>();
    private DenominationAdapter denominationAdapter;

    Denomination denominationSelected;

    public static TopUpFragmentStep2 newInstance() {
        Bundle args = new Bundle();
        TopUpFragmentStep2 fragment = new TopUpFragmentStep2();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_up_2, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (((TopUpAct) mContext).isVoucher) {
            linStep1.setVisibility(View.VISIBLE);
            linStep2.setVisibility(View.GONE);
        } else {
            linStep1.setVisibility(View.GONE);
            linStep2.setVisibility(View.VISIBLE);
        }
        initAdapter();
    }

    void setDenominationList(List<Denomination> list){
        denominationList.clear();
        denominationList.addAll(list);

        if (denominationAdapter != null){
            denominationAdapter.replaceItem(denominationList);
        }
    }

    private void initAdapter() {
        denominationAdapter = new DenominationAdapter(mContext, denominationList, this);
        rvContent.setLayoutManager(new GridLayoutManager(mContext, 2));
        rvContent.setAdapter(denominationAdapter);
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_next)
    void nextClicked() {
        ((TopUpAct) mContext).setNextPage();
    }

    @OnClick(R.id.btn_cancel)
    void cancelClicked() {
        ((TopUpAct) mContext).setPreviousPage();
    }

    @OnClick(R.id.btn_close)
    void closeClicked() {
        ((TopUpAct) mContext).finish();
    }

    /***** Implement Method *****/
    @Override
    public void onItemClicked(int position) {
        denominationSelected = denominationList.get(position);
    }
}

