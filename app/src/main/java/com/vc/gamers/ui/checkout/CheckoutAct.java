package com.vc.gamers.ui.checkout;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.VCGamersApp;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.base.BaseResponse;
import com.vc.gamers.model.response.checkout.Checkout;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.model.response.payment.PaymentMethod;
import com.vc.gamers.model.response.product.RememberMe;
import com.vc.gamers.model.response.profile.Profile;
import com.vc.gamers.persistence.Preferences;
import com.vc.gamers.presenter.PaymentMethodPresenter;
import com.vc.gamers.presenter.UserPresenter;
import com.vc.gamers.ui.dashboard.DashboardAct;
import com.vc.gamers.ui.newspromos.DetailPromoAct;
import com.vc.gamers.utils.Utils;
import com.vc.gamers.utils.handler.NetworkException;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by @erickrenata on 2019-12-13.
 */

public class CheckoutAct extends BaseActivity {

    public static final String CHECKOUT_PAYMENT_METHOD = "paymentMethod";
    public static final String CHECKOUT_USERNAME = "checkoutUsername";
    public static final String CHECKOUT_VA_NUMBERS = "checkoutVANumbers";
    public static final String CHECKOUT_PRODUCT = "checkoutProduct";
    public static final String CHECKOUT_ID = "checkoutID";
    public static final String CHECKOUT_GRAND_TOTAL = "grandTotal";
    public static final String CHECKOUT_CODE = "checkoutCode";
    public static final String CHECKOUT_ACCOUNT_NUMBER = "checkoutAccountNumber";
    public static final String CHECKOUT_IS_VOUCHER = "isVoucher";
    public static final String CHECKOUT_EMAIL = "isVoucher";
    public static final String CHECKOUT_DENOMINATION = "denomination";
    public static final String CHECKOUT_ADDITIONAL = "additional";

    @BindView(R.id.tv_checkout_using)
    TextView tvCheckoutUsing;
    @BindView(R.id.iv_payment_method)
    ImageView ivPaymentMethod;
    @BindView(R.id.tv_checkout_item)
    TextView tvCheckoutItem;
    @BindView(R.id.tv_checkout_username)
    TextView tvCheckoutUsername;
    @BindView(R.id.tv_checkout_price)
    TextView tvCheckoutPrice;
    @BindView(R.id.tv_checkout_price_total)
    TextView tvCheckoutPriceTotal;
    @BindView(R.id.et_phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.cb_remember_me)
    CheckBox cbRememberMe;
    @BindView(R.id.tv_terms)
    TextView tvTerms;
    @BindView(R.id.tv_additional)
    TextView tvAdditional;
    @BindView(R.id.tv_additional_name)
    TextView tvAdditionalName;

    @BindView(R.id.lin_additional)
    LinearLayout linAdditional;
    @BindView(R.id.lin_user_id)
    LinearLayout linUserID;
    @BindView(R.id.lin_email)
    LinearLayout linEmail;
    @BindView(R.id.lin_register)
    LinearLayout linRegister;

    Product product;
    PaymentMethod paymentMethod;
    String username;
    String additional;
    boolean isVoucher;
    Denomination denominationSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);

        product = getIntent().getParcelableExtra(CHECKOUT_PRODUCT);
        paymentMethod = getIntent().getParcelableExtra(CHECKOUT_PAYMENT_METHOD);
        username = getIntent().getStringExtra(CHECKOUT_USERNAME);
        isVoucher = getIntent().getBooleanExtra(CHECKOUT_IS_VOUCHER, false);
        denominationSelected = getIntent().getParcelableExtra(CHECKOUT_DENOMINATION);
        additional = getIntent().getStringExtra(CHECKOUT_ADDITIONAL);

        initView();
        if (Preferences.getProfileResponse() == null) {
            linRegister.setVisibility(View.VISIBLE);
            linEmail.setVisibility(View.VISIBLE);
        }
    }

    private void initView() {
        tvCheckoutUsing.setText("Checkout using " + paymentMethod.getName());
        tvCheckoutItem.setText(product.getName());
        tvCheckoutUsername.setText(username);
        tvCheckoutPrice.setText(Utils.convertNumberWithRp(paymentMethod.getPaymentDenomination().getPrice_item()));
        tvCheckoutPriceTotal.setText(Utils.convertNumberWithRp(paymentMethod.getPaymentDenomination().getPrice_item()));
        Picasso.get().load(paymentMethod.getImage()).into(ivPaymentMethod);
        if (denominationSelected.getDiscount() > 0) {
            if (denominationSelected.getArticle_id() != null) {
                tvTerms.setVisibility(View.VISIBLE);
            }
        }

        Profile profile = Preferences.getProfileResponse();
        if (profile != null) {
            if (profile.  getPhone() != null) {
                if (!profile.getPhone().isEmpty()) {
                    etPhoneNumber.setText(profile.getPhone());
                    etPhoneNumber.setSelection(etPhoneNumber.getText().length());

                    etEmail.setText(profile.getEmail());
                    etEmail.setSelection(etEmail.getText().length());
                }
            }
        }

        if (product.getHas_additional_field() == 1){
            linAdditional.setVisibility(View.VISIBLE);
//            tvAdditional.setText(Html.fromHtml(product.getNote_additional_field()));
            tvAdditional.setText("Server Number");
            tvAdditionalName.setText(additional);
        }

        if (isVoucher) {
            linUserID.setVisibility(View.GONE);
            linEmail.setVisibility(View.VISIBLE);
        } else {
            linUserID.setVisibility(View.VISIBLE);
            linEmail.setVisibility(View.GONE);
        }
    }

    private void saveRememberMe() {
        if (cbRememberMe.isChecked()) {
            List<RememberMe> rememberMeList = new ArrayList<>();
            if (Preferences.getRememberMe() != null) {
                rememberMeList.addAll(Preferences.getRememberMe());
            }
            rememberMeList.add(new RememberMe(product, username));
            Preferences.saveRememberMe(rememberMeList);
        }
    }

    private boolean validationForm() {
        if (etEmail.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_email_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Utils.isValidEmail(etEmail)) {
            Toast.makeText(this, "The email must be a valid email address.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etPassword.getText().toString().length() < 6) {
            Toast.makeText(this, "The password must be at least 6 characters.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_password_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_close)
    void close() {
        finish();
    }

    @OnClick(R.id.tv_terms)
    void showTerms() {
        startActivity(new Intent(this, DetailPromoAct.class)
                .putExtra("ID", denominationSelected.getArticle_id() + ""));
    }

    @OnClick(R.id.btn_confirm)
    void confirmCheckout() {
        if (Preferences.getLoginResponse() == null) {
            hideSoftKeyboard();
            if (validationForm()) {
                postRegister();
            }
        } else {
            showLoading();
            checkout();
        }
    }

    private void checkout(){
        String denominationId = denominationSelected.getId() + "";
        String productName = product.getName();
        String productType = "1"; // Dapet darimana datanya
        String amount = paymentMethod.getPaymentDenomination().getPrice_item() + "";
        String amountInCurrency = Utils.convertNumberWithRp(paymentMethod.getPaymentDenomination().getPrice_item());
        String subtotal = paymentMethod.getPaymentDenomination().getPrice_item() + "";
        String fee = "0"; // Dapet darimana datanya
        String grandTotal = paymentMethod.getPaymentDenomination().getPrice_item() + "";
        String reward = "1"; // Dapet darimana datanya
//        String paymentID = paymentMethod.getPaymentDenomination().getId() + "";
        String paymentID = paymentMethod.getId() + "";
        String paymentName = paymentMethod.getName();
        String paymentType = paymentMethod.getType();
        String paymentMethodCategoryID = paymentMethod.getPaymentDenomination().getId() + "";
        String userPlayerID = username;
        String additionalInformation = tvAdditional.getText().toString();
        String email = isVoucher ? etEmail.getText().toString() : (Preferences.getProfileResponse() != null? Preferences.getProfileResponse().getEmail() : etEmail.getText().toString());
        String phoneNumber = etPhoneNumber.getText().toString();

        postCheckout(denominationId, productName, productType, amount, amountInCurrency, subtotal, fee,
                grandTotal, reward, paymentID, paymentName, paymentType, paymentMethodCategoryID,
                userPlayerID, additionalInformation, email, phoneNumber);
    }


    /***** API Connection *****/
    private void postCheckout(String denomination_id, String product_name, String product_type,
                              String amount, String amount_in_currency, String subtotal,
                              String fee, String grand_total, String reward, String payment_id,
                              String payment_name, String payment_type, String payment_method_category_id,
                              String user_player_id, String additional_information, String email, String phoneNumber) {
        PaymentMethodPresenter.postCheckout(denomination_id, product_name, product_type, amount, amount_in_currency, subtotal, fee, grand_total, reward, payment_id, payment_name, payment_type, payment_method_category_id, user_player_id, additional_information, email, phoneNumber)
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    saveRememberMe();

                    Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();

                    boolean found = false;
                    String url = "";
                    if (response.getData().getPayment() != null) {
                        if (response.getData().getPayment().getActions() != null) {
                            if (response.getData().getPayment().getActions().size() > 0) {
                                for (Checkout.PaymentBean.ActionsBean actions : response.getData().getPayment().getActions()) {
                                    if (actions.getName().equalsIgnoreCase("deeplink-redirect")) {
                                        url = actions.getUrl();
                                        found = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    if (!found) {
                        String vaNumbers = "";
                        if (response.getData().getPayment().getVa_numbers() != null) {
                            vaNumbers = response.getData().getPayment().getVa_numbers().getVa_number();
                        }
                        startActivity(new Intent(this, CheckoutSummaryAct.class)
                                .putExtra(CHECKOUT_PAYMENT_METHOD, paymentMethod)
                                .putExtra(CHECKOUT_PRODUCT, product)
                                .putExtra(CHECKOUT_ID, response.getData().getOrder().getId())
                                .putExtra(CHECKOUT_GRAND_TOTAL, response.getData().getOrder().getGrand_total())
                                .putExtra(CHECKOUT_CODE, response.getData().getOrder().getPayment_code())
                                .putExtra(CHECKOUT_ACCOUNT_NUMBER, response.getData().getOrder().getPayment().getAccount_number())
                                .putExtra(CHECKOUT_USERNAME, isVoucher ? "" : username)
                                .putExtra(CHECKOUT_VA_NUMBERS, vaNumbers)
                                .putExtra(CHECKOUT_EMAIL, etEmail.getText().toString())
                                .putExtra(CHECKOUT_IS_VOUCHER, isVoucher)
                                .putExtra(CHECKOUT_ADDITIONAL, product.getHas_additional_field() == 1 ? additional : ""));
                    } else {
                        startActivity(new Intent(this, DashboardAct.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        try {
                            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(myIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(this, "No application can handle this request."
                                    + " Please install a webbrowser", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                }), Throwable::printStackTrace);
    }

    /***** API Connection *****/
    private void postRegister() {
        showLoading();
        UserPresenter.postRegister("", etEmail.getText().toString(), etPassword.getText().toString(), "")
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
//                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    Preferences.saveLoginResponse(response);
                    getProfile();
                }), throwable -> {
                    if (throwable instanceof HttpException) {
                        try {
                            HttpException httpException = (HttpException) throwable;
                            String res = httpException.response().errorBody().string();

                            Gson gson = new Gson();
                            BaseResponse baseResponse = gson.fromJson(res, BaseResponse.class);
                            NetworkException e = new NetworkException(baseResponse, httpException.response().code());
                            if (e.getResponse().isEmail_registered()) {
                                checkout();
                            }
                        } catch (Exception ignored) {

                        }
                    }
                });
    }

    private void getProfile() {
        UserPresenter.getProfile()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    Preferences.saveProfileRespose(response.getData());
                    checkout();
                }), Throwable::printStackTrace);
    }
}
