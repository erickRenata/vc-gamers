package com.vc.gamers.ui.newspromos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.santalu.autoviewpager.AutoViewPager;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.vc.gamers.R;
import com.vc.gamers.adapter.NewsPromosAdapter;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.article.Article;
import com.vc.gamers.model.response.banner.Banner;
import com.vc.gamers.presenter.ArticlePresenter;
import com.vc.gamers.presenter.BannerPresenter;
import com.vc.gamers.ui.banner.BannerFragment;
import com.vc.gamers.ui.filter.FilterArticleAct;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.vc.gamers.ui.filter.FilterArticleAct.TYPE_ALL;
import static com.vc.gamers.ui.filter.FilterArticleAct.TYPE_ARTICLE;
import static com.vc.gamers.ui.filter.FilterArticleAct.TYPE_NEWS;
import static com.vc.gamers.ui.filter.FilterArticleAct.TYPE_PROMO;
import static com.vc.gamers.utils.Utils.convertToPx;

/**
 * Created by @erickrenata on 2019-12-05.
 */

public class NewsPromosFragment extends BaseFragment {

    public static final int FILTER_ARTICLE_CODE = 100;

    @BindView(R.id.vp_banner)
    AutoViewPager vpBanner;
    @BindView(R.id.dots_indicator)
    DotsIndicator dotsIndicator;

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.lin_parent)
    RelativeLayout linParent;

    private NewsPromosAdapter newsPromosAdapter;
    private List<Article> newsPromosList = new ArrayList<>();
    private Context mContext;

    private List<BannerFragment> bannerFragmentList = new ArrayList<>();
    private String type;

    public static NewsPromosFragment newInstance() {
        Bundle args = new Bundle();
        NewsPromosFragment fragment = new NewsPromosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_promos, null);
        ButterKnife.bind(this, view);

        initBanner();
        initAdapter();

        getArticle();
        getBannerList();
        return view;
    }

    private void initAdapter() {
        rvContent.setFocusable(false);
        linParent.requestFocus();

        newsPromosAdapter = new NewsPromosAdapter(mContext, newsPromosList);
        rvContent.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        rvContent.setAdapter(newsPromosAdapter);

        swipeRefresh.setOnRefreshListener(() -> {
            if (type != null) {
                if (type.equalsIgnoreCase(TYPE_ALL)) {
                    getArticle();
                } else if (type.equalsIgnoreCase(TYPE_NEWS)) {
                    getNewsList();
                } else if (type.equalsIgnoreCase(TYPE_PROMO)) {
                    getPromoList();
                }
            } else {
                getArticle();
            }
        });
    }

    private void initBanner() {
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getChildFragmentManager());
        vpBanner.setAdapter(mAdapter);
        vpBanner.setClipToPadding(false);
        vpBanner.setPadding(convertToPx(16), 0, convertToPx(40), 0);
        vpBanner.setPageMargin(convertToPx(16));
        vpBanner.setOffscreenPageLimit(bannerFragmentList.size());
        vpBanner.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                if (position == 0) {
//                    vpBanner.setPadding(convertToPx(16), 0, convertToPx(40), 0);
//                } else
                if (position == bannerFragmentList.size() - 1) {
                    vpBanner.setPadding(convertToPx(40), 0, convertToPx(16), 0);

                } else {
//                    vpBanner.setPadding(convertToPx(28), 0, convertToPx(28), 0);
                    vpBanner.setPadding(convertToPx(16), 0, convertToPx(40), 0);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        dotsIndicator.setViewPager(vpBanner);
    }

    public void refreshData(){
        getArticle();
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_filter)
    void filterClicked() {
        startActivityForResult(new Intent(mContext, FilterArticleAct.class)
                .putExtra(TYPE_ARTICLE, type), FILTER_ARTICLE_CODE);
    }

    /***** API Connection *****/
    private void getArticle() {
        swipeRefresh.setRefreshing(true);
        ArticlePresenter.getArticle()
                .doOnTerminate(() -> getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    type = null;

                    newsPromosList.clear();
                    newsPromosList.addAll(response.getData());
                    newsPromosAdapter.replaceItem(newsPromosList);
                }), Throwable::printStackTrace);
    }

    private void getNewsList() {
        swipeRefresh.setRefreshing(true);
        ArticlePresenter.getNewsList()
                .doOnTerminate(() -> getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    newsPromosList.clear();
                    newsPromosList.addAll(response.getData());
                    newsPromosAdapter.replaceItem(newsPromosList);
                }), Throwable::printStackTrace);
    }

    private void getPromoList() {
        swipeRefresh.setRefreshing(true);
        ArticlePresenter.getPromoList()
                .doOnTerminate(() -> getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                    }
                }))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    newsPromosList.clear();
                    newsPromosList.addAll(response.getData());
                    newsPromosAdapter.replaceItem(newsPromosList);
                }), Throwable::printStackTrace);
    }

    private void getBannerList() {
//        showLoading();
        BannerPresenter.getBannerList()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    bannerFragmentList.clear();
                    for (Banner banner : response.getData()) {
                        bannerFragmentList.add(BannerFragment.newInstance(banner));
                    }
                    initBanner();
                }), Throwable::printStackTrace);
    }

    /***** Inner Class Adapter *****/
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return bannerFragmentList.get(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }

        @Override
        public int getCount() {
            return bannerFragmentList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    /***** Result Handler *****/
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILTER_ARTICLE_CODE) {
            if (resultCode == RESULT_OK) {
                type = data.getStringExtra("type");

                if (type != null) {
                    if (type.equalsIgnoreCase(TYPE_ALL)) {
                        getArticle();
                    } else if (type.equalsIgnoreCase(TYPE_NEWS)) {
                        getNewsList();
                    } else if (type.equalsIgnoreCase(TYPE_PROMO)) {
                        getPromoList();
                    }
                }
            }
        }
    }
}


