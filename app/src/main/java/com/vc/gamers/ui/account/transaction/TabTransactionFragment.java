package com.vc.gamers.ui.account.transaction;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.adapter.TransactionAdapter;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.history.Order;
import com.vc.gamers.presenter.HistoryPresenter;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by @erickrenata on 2019-12-13.
 */

public class TabTransactionFragment extends BaseFragment {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;

    private TransactionAdapter transactionAdapter;
    private List<Order> transactionList = new ArrayList<>();
    private Context mContext;

    public static TabTransactionFragment newInstance() {
        Bundle args = new Bundle();
        TabTransactionFragment fragment = new TabTransactionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_transaction, null);
        ButterKnife.bind(this, view);

        initAdapter();

        getPurchasedOrder();
        return view;
    }

    private void initAdapter() {
        transactionAdapter = new TransactionAdapter(mContext, transactionList);
        rvContent.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        rvContent.setAdapter(transactionAdapter);
    }

    /***** API Connection *****/
    private void getPurchasedOrder() {
//        showLoading();
        HistoryPresenter.getPurchasedOrder()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    transactionList.clear();
                    transactionList.addAll(response.getData());
                    transactionAdapter.replaceItem(transactionList);
                }), Throwable::printStackTrace);
    }
}


