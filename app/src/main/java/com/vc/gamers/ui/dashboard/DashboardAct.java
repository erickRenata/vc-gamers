package com.vc.gamers.ui.dashboard;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.ui.account.AccountFragment;
import com.vc.gamers.ui.gamesvouchers.GamesVouchersFragment;
import com.vc.gamers.ui.home.HomeFragment;
import com.vc.gamers.ui.newspromos.NewsPromosFragment;
import com.vc.gamers.ui.purchased.PurchasedFragment;
import com.vc.gamers.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.vc.gamers.ui.account.account.TabAccountFragment.PICK_FROM_CAMERA;
import static com.vc.gamers.ui.account.account.TabAccountFragment.PICK_FROM_GALLERY;

/**
 * Created by @erickrenata on 2019-12-04.
 */

public class DashboardAct extends BaseActivity {

    @BindView(R.id.iv_menu_home)
    ImageView ivHome;
    @BindView(R.id.iv_menu_games)
    ImageView ivGames;
    @BindView(R.id.iv_menu_news_promo)
    ImageView ivNewsPromo;
    @BindView(R.id.iv_menu_purchased)
    ImageView ivPurchased;
    @BindView(R.id.iv_menu_account)
    ImageView ivAccount;

    @BindView(R.id.frame_home)
    FrameLayout frameContainerHome;
    @BindView(R.id.frame_games)
    FrameLayout frameContainerGames;
    @BindView(R.id.frame_news_promo)
    FrameLayout frameContainerNewsPromo;
    @BindView(R.id.frame_purchased)
    FrameLayout frameContainerPurchased;
    @BindView(R.id.frame_account)
    FrameLayout frameContainerAccount;

    HomeFragment homeFragment = HomeFragment.newInstance();
    GamesVouchersFragment gamesFragment = GamesVouchersFragment.newInstance();
    NewsPromosFragment newsPromoFragment = NewsPromosFragment.newInstance();
    PurchasedFragment purchasedFragment = PurchasedFragment.newInstance();
    AccountFragment accountFragment = AccountFragment.newInstance();

    private Uri file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        initFrameContainer();
        menuHomeSelected();
    }

    private void initFrameContainer() {
        replaceFragment(R.id.frame_home, homeFragment);
        replaceFragment(R.id.frame_games, gamesFragment);
        replaceFragment(R.id.frame_news_promo, newsPromoFragment);
        replaceFragment(R.id.frame_purchased, purchasedFragment);
        replaceFragment(R.id.frame_account, accountFragment);
    }

    private void resetMenu() {
        ivHome.setImageResource(R.drawable.ic_menu_home_inactive);
        ivGames.setImageResource(R.drawable.ic_menu_games_inactive);
        ivNewsPromo.setImageResource(R.drawable.ic_menu_news_inactive);
        ivPurchased.setImageResource(R.drawable.ic_menu_purchase_inactive);
//        ivAccount.setImageResource(R.drawable.menua);

        frameContainerHome.setVisibility(View.GONE);
        frameContainerGames.setVisibility(View.GONE);
        frameContainerNewsPromo.setVisibility(View.GONE);
        frameContainerPurchased.setVisibility(View.GONE);
        frameContainerAccount.setVisibility(View.GONE);
    }

    private void menuHomeSelected() {
        resetMenu();
        ivHome.setImageResource(R.drawable.ic_menu_home_active);
        frameContainerHome.setVisibility(View.VISIBLE);
    }

    public void menuGamesSelected() {
        resetMenu();
        ivGames.setImageResource(R.drawable.ic_menu_games_active);
        frameContainerGames.setVisibility(View.VISIBLE);

        gamesFragment.refreshData();
    }

    private void menuNewsPromoSelected() {
        resetMenu();
        ivNewsPromo.setImageResource(R.drawable.ic_menu_news_active);
        frameContainerNewsPromo.setVisibility(View.VISIBLE);

        newsPromoFragment.refreshData();
    }

    private void menuPurchasedSelected() {
        resetMenu();
        ivPurchased.setImageResource(R.drawable.ic_menu_purchase_active);
        frameContainerPurchased.setVisibility(View.VISIBLE);

        purchasedFragment.refreshData();
    }

    private void menuAccountSelected() {
        resetMenu();
        frameContainerAccount.setVisibility(View.VISIBLE);
    }

    /***** On Click Method *****/
    @OnClick({R.id.btn_menu_home, R.id.btn_menu_games, R.id.btn_menu_news_promo, R.id.btn_menu_purchased, R.id.btn_menu_account})
    void menuSelected(View view) {
        switch (view.getId()) {
            case R.id.btn_menu_home:
                menuHomeSelected();
                break;
            case R.id.btn_menu_games:
                menuGamesSelected();
                break;
            case R.id.btn_menu_news_promo:
                menuNewsPromoSelected();
                break;
            case R.id.btn_menu_purchased:
                menuPurchasedSelected();
                break;
            case R.id.btn_menu_account:
                menuAccountSelected();
                break;
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
//            finish();
        super.onBackPressed();
//            return;
//        }
//
//        this.doubleBackToExitPressedOnce = true;
//
//        if (CommonUtils.isIndonesian()){
//            Toast.makeText(this, "Silahkan klik back sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();
//        }else {
//            Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();
//        }
//
//        new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);
    }

    /***** Result *****/
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PICK_FROM_CAMERA:
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//                    StrictMode.setVmPolicy(builder.build());
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    file = Uri.fromFile(Utils.getOutputMediaFile());
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
////                    accountFragment.tabAccountFragment.file = Uri.fromFile(Utils.getOutputMediaFile());
////                    intent.putExtra(MediaStore.EXTRA_OUTPUT, accountFragment.tabAccountFragment.file);
//                    startActivityForResult(intent, PICK_FROM_CAMERA);
//                } else {
//                    Toast.makeText(this, "Permission denied. Cannot open camera.", Toast.LENGTH_SHORT).show();
//                }
//                break;
//            case PICK_FROM_GALLERY:
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
//                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                    startActivityForResult(pickPhoto, PICK_FROM_GALLERY);
//                } else {
//                    Toast.makeText(this, "Permission denied. Cannot choose an image.", Toast.LENGTH_SHORT).show();
//                }
//                break;
//        }
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(imageReturnedIntent);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                accountFragment.tabAccountFragment.file = resultUri;
                accountFragment.tabAccountFragment.ivProfile.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }
//        switch (requestCode) {
//            case PICK_FROM_CAMERA:
//                if (resultCode == RESULT_OK) {
////                    accountFragment.tabAccountFragment.ivProfile.setImageURI(accountFragment.tabAccountFragment.file);
//                    accountFragment.tabAccountFragment.ivProfile.setImageURI(file);
//                }
//                break;
//            case PICK_FROM_GALLERY:
//                if (resultCode == RESULT_OK) {
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    accountFragment.tabAccountFragment.ivProfile.setImageURI(selectedImage);
//                }
//                break;
//        }
    }

}


