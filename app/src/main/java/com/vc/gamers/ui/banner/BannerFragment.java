package com.vc.gamers.ui.banner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.article.Article;
import com.vc.gamers.model.response.banner.Banner;
import com.vc.gamers.ui.newspromos.DetailPromoAct;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class BannerFragment extends BaseFragment {

    @BindView(R.id.iv_banner)
    ImageView ivBanner;

    Context mContext;

    Banner banner;

    public static BannerFragment newInstance(Banner banner) {
        Bundle args = new Bundle();
        BannerFragment fragment = new BannerFragment();
        args.putParcelable(Banner.class.getSimpleName(), banner);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_banner, null);
        ButterKnife.bind(this, view);

        banner = getArguments().getParcelable(Banner.class.getSimpleName());
        Picasso.get().load(banner.getImage()).into(ivBanner);
        return view;
    }

    @OnClick(R.id.iv_banner)
    void navigateToDetailBanner(){
        startActivity(new Intent(mContext, DetailPromoAct.class)
                .putExtra("ID", banner.getArticle_id() + ""));
    }


}

