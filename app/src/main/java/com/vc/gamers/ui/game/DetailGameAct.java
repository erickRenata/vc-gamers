package com.vc.gamers.ui.game;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.model.response.game.Denomination;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.presenter.ProductPresenter;
import com.vc.gamers.ui.topup.NewTopUpAct;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2019-12-09.
 */

public class DetailGameAct extends BaseActivity {

    @BindView(R.id.iv_games)
    ImageView ivGame;
    @BindView(R.id.iv_games_icon)
    ImageView ivGameIcon;
    @BindView(R.id.tv_game_title)
    TextView tvGameTitle;
    @BindView(R.id.tv_game_creator)
    TextView tvGameCreator;
    @BindView(R.id.tv_game_about_title)
    TextView tvGameAboutTitle;
    @BindView(R.id.tv_game_about)
    TextView tvGameAbout;

    private Product product;
    private ArrayList<Denomination> denominationList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_game);
        ButterKnife.bind(this);

        product = getIntent().getParcelableExtra(Product.class.getSimpleName());
        initView();

        getDetailProduct(product.getId() + "");
    }

    private void initView() {
        if (product == null) {
            return;
        }
        Picasso.get().load(product.getBanner())
                .placeholder(R.drawable.ic_placeholder_image)
                .into(ivGame);

        Picasso.get().load(product.getImage())
                .placeholder(R.drawable.ic_placeholder_image)
                .into(ivGameIcon);

        tvGameTitle.setText(product.getName());
        tvGameCreator.setText(product.getCompany());
        tvGameAboutTitle.setText("About " + product.getName());
        tvGameAboutTitle.setVisibility(View.GONE);
        tvGameAbout.setText(Html.fromHtml(product.getAbout_en()));
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_back)
    void backClicked() {
        onBackPressed();
    }

    @OnClick({R.id.btn_download_android, R.id.btn_download_ios})
    void downloadGame(View v) {
        String url = "";
        switch (v.getId()) {
            case R.id.btn_download_android:
                url = product.getAndroid_download();
                break;
            case R.id.btn_download_ios:
                url = product.getIos_download();
                break;
        }

        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No application can handle this request."
                    + " Please install a webbrowser", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_top_up)
    void topUpClicked() {
        startActivity(new Intent(this, NewTopUpAct.class)
                .putExtra("additional", product.getHas_additional_field())
                .putExtra(Product.class.getSimpleName(), product)
                .putExtra(Denomination.class.getSimpleName(), denominationList));
    }

    /***** API Connection *****/
    private void getDetailProduct(String id) {
        showLoading();
        ProductPresenter.getDetailProduct(id)
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    product = response.getData().getProduct();

                    denominationList.clear();
                    denominationList.addAll(response.getData().getDenomination());
                    initView();
                }), Throwable::printStackTrace);
    }
}
