package com.vc.gamers.ui.account;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.profile.Profile;
import com.vc.gamers.persistence.Preferences;
import com.vc.gamers.ui.account.account.TabAccountFragment;
import com.vc.gamers.ui.account.transaction.TabTransactionFragment;
import com.vc.gamers.ui.login.LoginAct;
import com.vc.gamers.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

/**
 * Created by @erickrenata on 2019-12-06.
 */

public class AccountFragment extends BaseFragment {

    @BindView(R.id.tabs_main)
    TabLayout tabLayoutAccount;
    @BindView(R.id.viewpager_main)
    ViewPager vpAccount;

    @BindView(R.id.tv_vcgamers_credit)
    TextView tvVCGamersCredit;
    @BindView(R.id.tv_vcgamers_points)
    TextView tvVCGamersPoints;
    @BindView(R.id.tv_gift_points)
    TextView tvGiftPoints;
    @BindView(R.id.lin_guest)
    LinearLayout linGuest;
    @BindView(R.id.lin_content)
    LinearLayout linContent;

    private Context mContext;

    public TabAccountFragment tabAccountFragment = TabAccountFragment.newInstance();
    public TabTransactionFragment tabTransactionFragment = TabTransactionFragment.newInstance();

    public static AccountFragment newInstance() {
        Bundle args = new Bundle();
        AccountFragment fragment = new AccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, null);
        ButterKnife.bind(this, view);

        if (Preferences.getLoginResponse() != null) {
            initView();
            initViewPager();
        } else {
            linContent.setVisibility(View.GONE);
            linGuest.setVisibility(View.VISIBLE);
        }
        return view;
    }

    private void initView() {
        Profile profile = Preferences.getProfileResponse();

        tvVCGamersCredit.setText(Utils.convertNumberWithoutCurrency(0));
        tvVCGamersPoints.setText(profile.getPoint() + "");
        tvVCGamersCredit.setText("0");
    }

    private void initViewPager() {
        vpAccount.setAdapter(new MyPagerAdapter(getChildFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT));
        tabLayoutAccount.setupWithViewPager(vpAccount);

        for (int i = 0; i < 2; i++) {
            TextView tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.text_tablayout, null);
            String title = "";
            switch (i) {
                case 0:
                    title = "Account";
                    break;
//                case 1:
//                    title = "Top Up";
//                    break;
                case 1:
                    title = "Transaction";
                    break;
            }
            tv.setText(title);
            tabLayoutAccount.getTabAt(i).setCustomView(tv);
        }
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_filter)
    void logout() {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    startActivity(new Intent(mContext, LoginAct.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                    // TODO : remove all memory
                    Preferences.removeLoginResponse();
                    Preferences.removeProfileResponse();
                    Preferences.removeRememberMe();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.dismiss();
                    break;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure want to sign out?")
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @OnClick(R.id.btn_login)
    void navigateToLogin() {
        startActivity(new Intent(mContext, LoginAct.class));
    }

    /***** Inner Class *****/
    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return tabAccountFragment;
//                case 1:
//                    return TabTopUpFragment.newInstance();
                case 1:
                    return tabTransactionFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            String title = "";
            switch (position) {
                case 0:
                    title = "Account";
                    break;
//                case 1:
//                    title = "Top Up";
//                    break;
                case 2:
                    title = "Transaction";
                    break;
            }
            return title;
        }
    }
}
