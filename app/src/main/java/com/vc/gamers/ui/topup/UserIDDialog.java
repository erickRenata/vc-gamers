package com.vc.gamers.ui.topup;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2020-01-31.
 */

public class UserIDDialog extends BaseDialogFragment {

    @BindView(R.id.iv_image_userid)
    ImageView ivImageUserId;

    public static UserIDDialog newInstance(String screenshoot_id) {
        Bundle args = new Bundle();
        UserIDDialog dialog = new UserIDDialog();
        args.putString("url", screenshoot_id);
        dialog.setArguments(args);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = getDefaultDialog();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image_userid);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this, dialog);

        String screenshotId = getArguments().getString("url");
        Picasso.get()
                .load(screenshotId)
                .into(ivImageUserId);
        return dialog;
    }

    public void show(FragmentManager fragmentManager) {
        try {
            fragmentManager.beginTransaction()
                    .add(this, UserIDDialog.class.getSimpleName())
                    .commitAllowingStateLoss();
        } catch (Exception ignored) {
        }
    }

    /***** On Click Handler *****/
    @OnClick(R.id.rl_user_id_dialog)
    void dismissDialog() {
        dismissAllowingStateLoss();
    }

}
