package com.vc.gamers.ui.forgotpass;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.vc.gamers.R;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.presenter.UserPresenter;
import com.vc.gamers.ui.register.RegisterAct;
import com.vc.gamers.utils.handler.RxUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2019-12-13.
 */

public class ForgotPasswordAct extends BaseActivity {

    @BindView(R.id.et_email)
    EditText etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    private boolean validationForm() {
        if (etEmail.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.error_email_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_register_now)
    void registerNowClicked() {
        startActivity(new Intent(this, RegisterAct.class));
        finish();
    }

    @OnClick(R.id.btn_send)
    void sendEmailClicked() {
        hideSoftKeyboard();
        if (validationForm()) {
            postForgotPassword();
        }
    }

    @OnClick(R.id.btn_sign_in)
    void signInClicked() {
        finish();
    }

    /***** API Connection *****/
    private void postForgotPassword() {
        showLoading();
        UserPresenter.postForgotPassword(etEmail.getText().toString())
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    finish();

                    Toast.makeText(this, response.getMessage(), Toast.LENGTH_SHORT).show();
                }), Throwable::printStackTrace);
    }
}