package com.vc.gamers.ui.newspromos;

import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vc.gamers.R;
import com.vc.gamers.base.BaseActivity;
import com.vc.gamers.model.response.article.Article;
import com.vc.gamers.presenter.ArticlePresenter;
import com.vc.gamers.presenter.ProductPresenter;
import com.vc.gamers.utils.handler.RxUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2020-01-22.
 */

public class DetailPromoAct extends BaseActivity {

    @BindView(R.id.iv_games)
    ImageView ivGame;
    @BindView(R.id.tv_promo_title)
    TextView tvPromoTitle;
    @BindView(R.id.tv_promo_desc)
    TextView tvPromoDesc;

    private Article promo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_promo);
        ButterKnife.bind(this);

        promo = getIntent().getParcelableExtra(Article.class.getSimpleName());
        if (promo != null){
            initView();
        }else {
            String id = getIntent().getStringExtra("ID");
            getDetailProduct(id);
        }
    }

    private void initView() {
        Picasso.get().load(promo.getImage())
                .placeholder(R.drawable.ic_placeholder_image)
                .into(ivGame);

        tvPromoTitle.setText(promo.getTitle());
        tvPromoDesc.setText(Html.fromHtml(promo.getDescription_en()));
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_back)
    void backClicked() {
        onBackPressed();
    }

    /***** API Connection *****/
    private void getDetailProduct(String id) {
        showLoading();
        ArticlePresenter.getDetailArticle(id)
                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> runOnUiThread(() -> {
                    promo = response.getData();
                    initView();
                }), Throwable::printStackTrace);
    }
}
