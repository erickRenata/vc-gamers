package com.vc.gamers.ui.account.topup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vc.gamers.R;
import com.vc.gamers.adapter.TopUpPaymentAdapter;
import com.vc.gamers.base.BaseFragment;
import com.vc.gamers.model.response.payment.Payment;
import com.vc.gamers.presenter.PaymentMethodPresenter;
import com.vc.gamers.ui.topup.NewTopUpAct;
import com.vc.gamers.utils.handler.RxUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by @erickrenata on 2019-12-13.
 */

public class TabTopUpFragment extends BaseFragment {

    private Context mContext;

    @BindView(R.id.rv_content)
    RecyclerView rvContent;

    private TopUpPaymentAdapter topUpPaymentAdapter;
    private List<Payment> paymentList = new ArrayList<>();

    public static TabTopUpFragment newInstance() {
        Bundle args = new Bundle();
        TabTopUpFragment fragment = new TabTopUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_top_up, null);
        ButterKnife.bind(this, view);

        initAdapter();

        getPaymentMethodList();
        return view;
    }

    private void initAdapter() {
        topUpPaymentAdapter = new TopUpPaymentAdapter(mContext, paymentList);
        rvContent.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
        rvContent.setAdapter(topUpPaymentAdapter);
    }

    /***** On Click Handler *****/
    @OnClick(R.id.btn_buy_credit)
    void buyCreditClicked() {
        startActivity(new Intent(mContext, NewTopUpAct.class));
    }

    /***** API Connection *****/
    private void getPaymentMethodList() {
//        showLoading();
        PaymentMethodPresenter.getPaymentMethodList()
//                .doOnTerminate(() -> runOnUiThread(this::dismissLoading))
                .compose(RxUtils.applyApiCall())
                .subscribe(response -> getActivity().runOnUiThread(() -> {
                    paymentList.clear();
                    paymentList.addAll(response.getData());
                    topUpPaymentAdapter.replaceItem(paymentList);
                }), Throwable::printStackTrace);
    }
}

