package com.vc.gamers.model.response.game;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by @erickrenata on 2020-01-24.
 */

public class PaymentDenomination implements Parcelable {

    /**
     * id : 1
     * price_item : 10000
     * name : BCA
     * image : https://cdn-vcgamers.developerrizki.online/payment_method/527af50680c7d091a59e9128147eba051580703653.png
     * type : manual
     * status : 1
     * fee : 0
     * is_fee_percentage : 0
     * account_name : Admin VCGAME
     * account_number : 1234567890
     * payment_method_category_id : 1
     * payment_method_category_image : https://cdn-vcgamers.developerrizki.online/payment_category/f9aa2cc20f784aef4ac1dded765ff848.svg
     */

    private int id;
    private int price_item;
    private String name;
    private String image;
    private String type;
    private int status;
    private double fee;
    private int is_fee_percentage;
    private String account_name;
    private String account_number;
    private int payment_method_category_id;
    private String payment_method_category_image;

    protected PaymentDenomination(Parcel in) {
        id = in.readInt();
        price_item = in.readInt();
        name = in.readString();
        image = in.readString();
        type = in.readString();
        status = in.readInt();
        fee = in.readDouble();
        is_fee_percentage = in.readInt();
        account_name = in.readString();
        account_number = in.readString();
        payment_method_category_id = in.readInt();
        payment_method_category_image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(price_item);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(type);
        dest.writeInt(status);
        dest.writeDouble(fee);
        dest.writeInt(is_fee_percentage);
        dest.writeString(account_name);
        dest.writeString(account_number);
        dest.writeInt(payment_method_category_id);
        dest.writeString(payment_method_category_image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaymentDenomination> CREATOR = new Creator<PaymentDenomination>() {
        @Override
        public PaymentDenomination createFromParcel(Parcel in) {
            return new PaymentDenomination(in);
        }

        @Override
        public PaymentDenomination[] newArray(int size) {
            return new PaymentDenomination[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice_item() {
        return price_item;
    }

    public void setPrice_item(int price_item) {
        this.price_item = price_item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public int getIs_fee_percentage() {
        return is_fee_percentage;
    }

    public void setIs_fee_percentage(int is_fee_percentage) {
        this.is_fee_percentage = is_fee_percentage;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public int getPayment_method_category_id() {
        return payment_method_category_id;
    }

    public void setPayment_method_category_id(int payment_method_category_id) {
        this.payment_method_category_id = payment_method_category_id;
    }

    public String getPayment_method_category_image() {
        return payment_method_category_image;
    }

    public void setPayment_method_category_image(String payment_method_category_image) {
        this.payment_method_category_image = payment_method_category_image;
    }
}
