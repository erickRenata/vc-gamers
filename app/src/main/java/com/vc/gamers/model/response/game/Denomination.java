package com.vc.gamers.model.response.game;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by @erickrenata on 2020-01-24.
 */

public class Denomination implements Parcelable {

    /**
     * id : 20
     * product_id : 2
     * amount_in_currency : 33 Garena Shells
     * price : 10000
     * stock : 100
     * discount : 0
     * is_discount_percentage : 0
     * reward : 10
     * is_reward_percentage : 0
     * article_id : null
     * status : 1
     * user_id : 1
     * created_at : 2020-02-17 22:17:13
     * updated_at : 2020-02-17 22:17:13
     * deleted_at : null
     * payment : [{"id":1,"price_item":10000,"name":"BCA","image":"https://cdn-vcgamers.developerrizki.online/payment_method/527af50680c7d091a59e9128147eba051580703653.png","type":"manual","status":1,"fee":0,"is_fee_percentage":0,"account_name":"Admin VCGAME","account_number":"1234567890","payment_method_category_id":1,"payment_method_category_image":"https://cdn-vcgamers.developerrizki.online/payment_category/f9aa2cc20f784aef4ac1dded765ff848.svg"},{"id":2,"price_item":14000,"name":"Permata Virtual Account","image":"https://cdn-vcgamers.developerrizki.online/payment_method/0a073aafa9ef963380416dfaeb74f7cd1575384018.jpeg","type":"midtrans","status":1,"fee":4000,"is_fee_percentage":0,"account_name":null,"account_number":null,"payment_method_category_id":2,"payment_method_category_image":"https://cdn-vcgamers.developerrizki.online/payment_category/8cfc34b28ce68a5a9c96cfbe9145aa96.svg"},{"id":4,"price_item":10200,"name":"GO-PAY","image":"https://cdn-vcgamers.developerrizki.online/payment_method/ef03f9934b122ad8677dd2450ddcb9d11581663523.png","type":"midtrans","status":1,"fee":2,"is_fee_percentage":1,"account_name":null,"account_number":null,"payment_method_category_id":2,"payment_method_category_image":"https://cdn-vcgamers.developerrizki.online/payment_category/8cfc34b28ce68a5a9c96cfbe9145aa96.svg"},{"id":5,"price_item":14000,"name":"Mandiri Bill Payment","image":"https://cdn-vcgamers.developerrizki.online/payment_method/dba04fcc7f8c3f162b99a9d3d79f7b291580784476.png","type":"midtrans","status":1,"fee":4000,"is_fee_percentage":0,"account_name":null,"account_number":null,"payment_method_category_id":2,"payment_method_category_image":"https://cdn-vcgamers.developerrizki.online/payment_category/8cfc34b28ce68a5a9c96cfbe9145aa96.svg"},{"id":6,"price_item":14000,"name":"BNI Virtual Account","image":"https://cdn-vcgamers.developerrizki.online/payment_method/411f1bd6cf5998f12aa18fd8e847fba61580784494.png","type":"midtrans","status":1,"fee":4000,"is_fee_percentage":0,"account_name":null,"account_number":null,"payment_method_category_id":2,"payment_method_category_image":"https://cdn-vcgamers.developerrizki.online/payment_category/8cfc34b28ce68a5a9c96cfbe9145aa96.svg"},{"id":7,"price_item":14000,"name":"BCA Virtual Account","image":"https://cdn-vcgamers.developerrizki.online/payment_method/527af50680c7d091a59e9128147eba051581665132.png","type":"midtrans","status":1,"fee":4000,"is_fee_percentage":0,"account_name":null,"account_number":null,"payment_method_category_id":2,"payment_method_category_image":"https://cdn-vcgamers.developerrizki.online/payment_category/8cfc34b28ce68a5a9c96cfbe9145aa96.svg"}]
     */

    private int id;
    private int product_id;
    private String amount_in_currency;
    private int price;
    private int stock;
    private int discount;
    private int is_discount_percentage;
    private int reward;
    private int is_reward_percentage;
    private Integer article_id;
    private int status;
    private int user_id;
    private String created_at;
    private String updated_at;
    private String deleted_at;
    private List<PaymentDenomination> payment;
    private boolean is_selected;

    protected Denomination(Parcel in) {
        id = in.readInt();
        product_id = in.readInt();
        amount_in_currency = in.readString();
        price = in.readInt();
        stock = in.readInt();
        discount = in.readInt();
        is_discount_percentage = in.readInt();
        reward = in.readInt();
        is_reward_percentage = in.readInt();
        if (in.readByte() == 0) {
            article_id = null;
        } else {
            article_id = in.readInt();
        }
        status = in.readInt();
        user_id = in.readInt();
        created_at = in.readString();
        updated_at = in.readString();
        deleted_at = in.readString();
        payment = in.createTypedArrayList(PaymentDenomination.CREATOR);
        is_selected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(product_id);
        dest.writeString(amount_in_currency);
        dest.writeInt(price);
        dest.writeInt(stock);
        dest.writeInt(discount);
        dest.writeInt(is_discount_percentage);
        dest.writeInt(reward);
        dest.writeInt(is_reward_percentage);
        if (article_id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(article_id);
        }
        dest.writeInt(status);
        dest.writeInt(user_id);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(deleted_at);
        dest.writeTypedList(payment);
        dest.writeByte((byte) (is_selected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Denomination> CREATOR = new Creator<Denomination>() {
        @Override
        public Denomination createFromParcel(Parcel in) {
            return new Denomination(in);
        }

        @Override
        public Denomination[] newArray(int size) {
            return new Denomination[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getAmount_in_currency() {
        return amount_in_currency;
    }

    public void setAmount_in_currency(String amount_in_currency) {
        this.amount_in_currency = amount_in_currency;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getIs_discount_percentage() {
        return is_discount_percentage;
    }

    public void setIs_discount_percentage(int is_discount_percentage) {
        this.is_discount_percentage = is_discount_percentage;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public int getIs_reward_percentage() {
        return is_reward_percentage;
    }

    public void setIs_reward_percentage(int is_reward_percentage) {
        this.is_reward_percentage = is_reward_percentage;
    }

    public Integer getArticle_id() {
        return article_id;
    }

    public void setArticle_id(Integer article_id) {
        this.article_id = article_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public List<PaymentDenomination> getPayment() {
        return payment;
    }

    public void setPayment(List<PaymentDenomination> payment) {
        this.payment = payment;
    }

    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }
}
