package com.vc.gamers.model.response.product;

/**
 * Created by @erickrenata on 2020-01-27.
 */

public class Category {

    /**
     * id : 2
     * name : PC Desktop
     * status : 1
     * user_id : 1
     * created_at : 2019-11-14 12:27:13
     * updated_at : 2020-01-17 11:00:33
     * deleted_at : null
     */

    private int id;
    private String name;
    private int status;
    private int user_id;
    private String created_at;
    private String updated_at;
    private Object deleted_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Object getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }
}
