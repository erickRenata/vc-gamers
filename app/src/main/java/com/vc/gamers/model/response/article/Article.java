package com.vc.gamers.model.response.article;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by @erickrenata on 2020-01-10.
 */

public class Article implements Parcelable {

    /**
     * id : 5
     * title : Test Berita Ku
     * title_en : Test Berita Ku
     * image : [PATH_CDN]/article/511c97baf1b66a2da1a4477c2aa526f9.jpeg
     * type : news
     * description : Test Berita
     * description_en : Test Berita
     * view : 0
     * user_id : 1
     * created_at : 2019-11-17 10:39:22
     * updated_at : 2019-11-17 10:46:12
     */

    private int id;
    private String title;
    private String title_en;
    private String image;
    private String type;
    private String description;
    private String description_en;
    private int view;
    private int user_id;
    private String created_at;
    private String updated_at;

    protected Article(Parcel in) {
        id = in.readInt();
        title = in.readString();
        title_en = in.readString();
        image = in.readString();
        type = in.readString();
        description = in.readString();
        description_en = in.readString();
        view = in.readInt();
        user_id = in.readInt();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(title_en);
        dest.writeString(image);
        dest.writeString(type);
        dest.writeString(description);
        dest.writeString(description_en);
        dest.writeInt(view);
        dest.writeInt(user_id);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
