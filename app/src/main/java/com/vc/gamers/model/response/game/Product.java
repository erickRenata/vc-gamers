package com.vc.gamers.model.response.game;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by @erickrenata on 2020-01-20.
 */

public class Product implements Parcelable {

    /**
     * id : 4
     * name : Mobile Legends Bang Bang
     * company : Moonton
     * website : https://www.mobilelegends.com/
     * community : https://www.facebook.com/MobileLegendsGameMalaysia/?country=en&brand_redir=1212826968780074
     * image : https://cdn-vcgamers.developerrizki.online/product/e0437caac0cede4e0ff01720370a4113.png
     * screenshoot_id : https://cdn-vcgamers.developerrizki.online/product/a346b0966bab03b3c28898a450ba35c6.jpeg
     * banner : https://cdn-vcgamers.developerrizki.online/product/4835cc2b366b5617cc997a7f8dde8223.png
     * android_download : https://play.google.com/store/apps/details?id=com.mobile.legends
     * ios_download : https://itunes.apple.com/app/id1160056295?country=en
     * about : <p style="box-sizing: border-box; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 11px; color: rgb(136, 136, 136); font-family: roboto, &quot;helvetica neue&quot;, Helvetica, Arial, sans-serif; text-align: justify; background-color: rgb(244, 244, 244);"><span style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0); font-family: -apple-system, system-ui, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif;">Tentang Mobile Legends: Bang Bang:&nbsp;</span><br></p><p style="box-sizing: border-box; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 11px;">Merupakan salah satu game MOBA paling populer saat ini, Mobile Legends: Bang Bang bisa dimainkan oleh 10 pemain yang kemudian dibagi menjadi 2 tim. Lewat mode 5v5, kamu bisa memilih Hero andalan dan atur strategi dengan tim untuk menaklukan tim lawan.</p>
     * about_en : <p style="box-sizing: border-box; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 11px; color: rgb(136, 136, 136); font-family: roboto, &quot;helvetica neue&quot;, Helvetica, Arial, sans-serif; text-align: justify; background-color: rgb(244, 244, 244);"><span style="background-color: rgb(255, 255, 255); color: rgb(0, 0, 0); font-family: -apple-system, system-ui, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif;">Tentang Mobile Legends: Bang Bang:&nbsp;</span><br></p><p style="box-sizing: border-box; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; font-size: 11px;">Merupakan salah satu game MOBA paling populer saat ini, Mobile Legends: Bang Bang bisa dimainkan oleh 10 pemain yang kemudian dibagi menjadi 2 tim. Lewat mode 5v5, kamu bisa memilih Hero andalan dan atur strategi dengan tim untuk menaklukan tim lawan.</p>
     * terms_and_condition : null
     * terms_and_condition_en : null
     * note : null
     * note_en : null
     * currency_name : IDR
     * status : 1
     * has_additional_field : 1
     * note_additional_field : <p>ID Zona</p>
     * note_additional_field_en : <p>Server ID</p>
     * category_id : 3
     * product_type_id : 1
     * genre_id : 1
     * user_id : 1
     * created_at : 2020-02-24 20:41:59
     * updated_at : 2020-03-18 11:34:51
     */

    private int id;
    private String name;
    private String company;
    private String website;
    private String community;
    private String image;
    private String screenshoot_id;
    private String banner;
    private String android_download;
    private String ios_download;
    private String about;
    private String about_en;
    private String terms_and_condition;
    private String terms_and_condition_en;
    private String note;
    private String note_en;
    private String currency_name;
    private int status;
    private int has_additional_field;
    private String note_additional_field;
    private String note_additional_field_en;
    private int category_id;
    private int product_type_id;
    private String genre_id;
    private int user_id;
    private String created_at;
    private String updated_at;

    protected Product(Parcel in) {
        id = in.readInt();
        name = in.readString();
        company = in.readString();
        website = in.readString();
        community = in.readString();
        image = in.readString();
        screenshoot_id = in.readString();
        banner = in.readString();
        android_download = in.readString();
        ios_download = in.readString();
        about = in.readString();
        about_en = in.readString();
        currency_name = in.readString();
        status = in.readInt();
        has_additional_field = in.readInt();
        note_additional_field = in.readString();
        note_additional_field_en = in.readString();
        category_id = in.readInt();
        product_type_id = in.readInt();
        genre_id = in.readString();
        user_id = in.readInt();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(company);
        dest.writeString(website);
        dest.writeString(community);
        dest.writeString(image);
        dest.writeString(screenshoot_id);
        dest.writeString(banner);
        dest.writeString(android_download);
        dest.writeString(ios_download);
        dest.writeString(about);
        dest.writeString(about_en);
        dest.writeString(currency_name);
        dest.writeInt(status);
        dest.writeInt(has_additional_field);
        dest.writeString(note_additional_field);
        dest.writeString(note_additional_field_en);
        dest.writeInt(category_id);
        dest.writeInt(product_type_id);
        dest.writeString(genre_id);
        dest.writeInt(user_id);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getScreenshoot_id() {
        return screenshoot_id;
    }

    public void setScreenshoot_id(String screenshoot_id) {
        this.screenshoot_id = screenshoot_id;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getAndroid_download() {
        return android_download;
    }

    public void setAndroid_download(String android_download) {
        this.android_download = android_download;
    }

    public String getIos_download() {
        return ios_download;
    }

    public void setIos_download(String ios_download) {
        this.ios_download = ios_download;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAbout_en() {
        return about_en;
    }

    public void setAbout_en(String about_en) {
        this.about_en = about_en;
    }

    public String getTerms_and_condition() {
        return terms_and_condition;
    }

    public void setTerms_and_condition(String terms_and_condition) {
        this.terms_and_condition = terms_and_condition;
    }

    public String getTerms_and_condition_en() {
        return terms_and_condition_en;
    }

    public void setTerms_and_condition_en(String terms_and_condition_en) {
        this.terms_and_condition_en = terms_and_condition_en;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote_en() {
        return note_en;
    }

    public void setNote_en(String note_en) {
        this.note_en = note_en;
    }

    public String getCurrency_name() {
        return currency_name;
    }

    public void setCurrency_name(String currency_name) {
        this.currency_name = currency_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getHas_additional_field() {
        return has_additional_field;
    }

    public void setHas_additional_field(int has_additional_field) {
        this.has_additional_field = has_additional_field;
    }

    public String getNote_additional_field() {
        return note_additional_field;
    }

    public void setNote_additional_field(String note_additional_field) {
        this.note_additional_field = note_additional_field;
    }

    public String getNote_additional_field_en() {
        return note_additional_field_en;
    }

    public void setNote_additional_field_en(String note_additional_field_en) {
        this.note_additional_field_en = note_additional_field_en;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getProduct_type_id() {
        return product_type_id;
    }

    public void setProduct_type_id(int product_type_id) {
        this.product_type_id = product_type_id;
    }

    public String getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(String genre_id) {
        this.genre_id = genre_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}