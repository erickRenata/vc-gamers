package com.vc.gamers.model.response.product;

import com.vc.gamers.model.response.game.Product;

/**
 * Created by @erickrenata on 2020-01-31.
 */

public class RememberMe {

    private Product product;
    private String userId;

    public RememberMe(Product product, String userId) {
        this.product = product;
        this.userId = userId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
