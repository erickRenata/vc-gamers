package com.vc.gamers.model.response.payment;

import android.os.Parcel;
import android.os.Parcelable;

import com.vc.gamers.model.response.game.PaymentDenomination;

/**
 * Created by @erickrenata on 2020-01-10.
 */

public class PaymentMethod implements Parcelable {
    /**
     * id : 1
     * name : Pembayaran Manual
     * image : [PATH_CDN]/payment/e78c7c66f7e3466ed59a68c3e9b1e7761575383999.jpeg
     * type : manual
     * status : 1
     */

    private int id;
    private String name;
    private String image;
    private String type;
    private int status;
    private PaymentDenomination paymentDenomination;
    private boolean selected;

    protected PaymentMethod(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readString();
        type = in.readString();
        status = in.readInt();
        paymentDenomination = in.readParcelable(PaymentDenomination.class.getClassLoader());
        selected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(type);
        dest.writeInt(status);
        dest.writeParcelable(paymentDenomination, flags);
        dest.writeByte((byte) (selected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PaymentMethod> CREATOR = new Creator<PaymentMethod>() {
        @Override
        public PaymentMethod createFromParcel(Parcel in) {
            return new PaymentMethod(in);
        }

        @Override
        public PaymentMethod[] newArray(int size) {
            return new PaymentMethod[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public PaymentDenomination getPaymentDenomination() {
        return paymentDenomination;
    }

    public void setPaymentDenomination(PaymentDenomination paymentDenomination) {
        this.paymentDenomination = paymentDenomination;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
