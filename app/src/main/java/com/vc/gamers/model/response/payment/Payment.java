package com.vc.gamers.model.response.payment;

import java.util.List;

/**
 * Created by @erickrenata on 2020-01-10.
 */

public class Payment {

    /**
     * name : Manual
     * methods : [{"id":1,"name":"Pembayaran Manual","image":"[PATH_CDN]/payment/e78c7c66f7e3466ed59a68c3e9b1e7761575383999.jpeg","type":"manual","status":1}]
     */

    private String name;
    private String image;
    private List<PaymentMethod> methods;
    private boolean isSelected;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PaymentMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<PaymentMethod> methods) {
        this.methods = methods;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
