package com.vc.gamers.model.response.product;

/**
 * Created by @erickrenata on 2020-01-27.
 */

public class ModelText {

    /**
     * id : 1
     * name : Flash Top Up
     */

    private String id;
    private String name;
    private int type;
    private boolean selected;

    public ModelText(String id, String name, int type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
