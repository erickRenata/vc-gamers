package com.vc.gamers.model.response.profile;

/**
 * Created by @erickrenata on 2020-01-09.
 */

public class Profile {

    /**
     * id : 5
     * name : devel
     * email : devel@mail.com
     * level_id : 3
     * phone : 081320301131
     * sex : L
     * status : 1
     * token_fcm : null
     * image :
     * avatar :
     * point : 0
     * created_at : 2020-01-07 17:29:19
     * updated_at : 2020-01-07 17:29:19
     */

    private int id;
    private String name;
    private String email;
    private int level_id;
    private String phone;
    private String sex;
    private int status;
    private Object token_fcm;
    private String image;
    private String avatar;
    private String birth_date;
    private int point;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getLevel_id() {
        return level_id;
    }

    public void setLevel_id(int level_id) {
        this.level_id = level_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getToken_fcm() {
        return token_fcm;
    }

    public void setToken_fcm(Object token_fcm) {
        this.token_fcm = token_fcm;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }
}
