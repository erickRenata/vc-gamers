package com.vc.gamers.model.response.product;

/**
 * Created by @erickrenata on 2020-01-27.
 */

public class Type {

    /**
     * id : 1
     * type : Flash Top Up
     * status : 1
     * created_at : null
     * updated_at : null
     */

    private int id;
    private String type;
    private int status;
    private Object created_at;
    private Object updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Object created_at) {
        this.created_at = created_at;
    }

    public Object getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Object updated_at) {
        this.updated_at = updated_at;
    }
}
