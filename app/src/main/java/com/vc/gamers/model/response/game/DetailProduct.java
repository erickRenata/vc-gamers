package com.vc.gamers.model.response.game;

import java.util.List;

/**
 * Created by @erickrenata on 2020-01-24.
 */

public class DetailProduct {

    private Product product;
    private List<Denomination> denomination;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Denomination> getDenomination() {
        return denomination;
    }

    public void setDenomination(List<Denomination> denomination) {
        this.denomination = denomination;
    }
}
