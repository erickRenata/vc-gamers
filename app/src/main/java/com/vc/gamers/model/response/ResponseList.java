package com.vc.gamers.model.response;

import com.vc.gamers.base.BaseResponse;

import java.util.List;

/**
 * Created by @erickrenata on 2020-01-09.
 */

public class ResponseList<T> extends BaseResponse {

    private List<T> data;

    public List<T> getData() {
        return data;
    }
}

