package com.vc.gamers.model.response.checkout;

import java.util.List;

/**
 * Created by @erickrenata on 2020-02-26.
 */

public class Checkout {

    /**
     * order : {"id":"VCGAME-ORD-1582691083000","date":"2020-02-26 11:24:43","user_id":5,"denomination_id":"27","product_name":"Call Of Duty: Mobile","product_type":1,"amount":10200,"amount_in_currency":"IDR 10200","subtotal":10000,"fee":200,"grand_total":10200,"reward":10,"payment_id":"4","payment_name":"GO-PAY","payment_type":"gopay","user_player_id":"a","additional_information":"","product_code":"","status":4,"email":"devel@mail.com","phone_number":"081321468127","payment_code":0,"updated_at":"2020-02-26 23:24:44","created_at":"2020-02-26 23:24:44"}
     * payment : {"status_code":"201","status_message":"GO-PAY transaction is created","transaction_id":"83ce2c45-6132-4b14-ad9d-d9765518e6b4","order_id":"VCGAME-ORD-1582691083000","merchant_id":"G184637003","gross_amount":"10200.00","currency":"IDR","payment_type":"gopay","transaction_time":"2020-02-26 23:24:44","transaction_status":"pending","fraud_status":"accept","actions":[{"name":"generate-qr-code","method":"GET","url":"https://api.sandbox.veritrans.co.id/v2/gopay/83ce2c45-6132-4b14-ad9d-d9765518e6b4/qr-code"},{"name":"deeplink-redirect","method":"GET","url":"https://simulator.sandbox.midtrans.com/gopay/ui/checkout?referenceid=p1c1xBuTRo&callback_url=http%3A%2F%2Fvcgamers.com%3Forder_id%3DVCGAME-ORD-1582691083000"},{"name":"get-status","method":"GET","url":"https://api.sandbox.veritrans.co.id/v2/83ce2c45-6132-4b14-ad9d-d9765518e6b4/status"},{"name":"cancel","method":"POST","url":"https://api.sandbox.veritrans.co.id/v2/83ce2c45-6132-4b14-ad9d-d9765518e6b4/cancel"}]}
     */

    private OrderBean order;
    private PaymentBean payment;

    public OrderBean getOrder() {
        return order;
    }

    public void setOrder(OrderBean order) {
        this.order = order;
    }

    public PaymentBean getPayment() {
        return payment;
    }

    public void setPayment(PaymentBean payment) {
        this.payment = payment;
    }

    public static class OrderBean {
        /**
         * id : VCGAME-ORD-1582691083000
         * date : 2020-02-26 11:24:43
         * user_id : 5
         * denomination_id : 27
         * product_name : Call Of Duty: Mobile
         * product_type : 1
         * amount : 10200
         * amount_in_currency : IDR 10200
         * subtotal : 10000
         * fee : 200
         * grand_total : 10200
         * reward : 10
         * payment_id : 4
         * payment_name : GO-PAY
         * payment_type : gopay
         * user_player_id : a
         * additional_information :
         * product_code :
         * status : 4
         * email : devel@mail.com
         * phone_number : 081321468127
         * payment_code : 0
         * updated_at : 2020-02-26 23:24:44
         * created_at : 2020-02-26 23:24:44
         */

        private String id;
        private String date;
        private int user_id;
        private String denomination_id;
        private String product_name;
        private int product_type;
        private int amount;
        private String amount_in_currency;
        private int subtotal;
        private int fee;
        private int grand_total;
        private int reward;
        private String payment_id;
        private String payment_name;
        private String payment_type;
        private String user_player_id;
        private String additional_information;
        private String product_code;
        private int status;
        private String email;
        private String phone_number;
        private int payment_code;
        private String updated_at;
        private String created_at;
        private PaymentChildBean payment;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getDenomination_id() {
            return denomination_id;
        }

        public void setDenomination_id(String denomination_id) {
            this.denomination_id = denomination_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public int getProduct_type() {
            return product_type;
        }

        public void setProduct_type(int product_type) {
            this.product_type = product_type;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getAmount_in_currency() {
            return amount_in_currency;
        }

        public void setAmount_in_currency(String amount_in_currency) {
            this.amount_in_currency = amount_in_currency;
        }

        public int getSubtotal() {
            return subtotal;
        }

        public void setSubtotal(int subtotal) {
            this.subtotal = subtotal;
        }

        public int getFee() {
            return fee;
        }

        public void setFee(int fee) {
            this.fee = fee;
        }

        public int getGrand_total() {
            return grand_total;
        }

        public void setGrand_total(int grand_total) {
            this.grand_total = grand_total;
        }

        public int getReward() {
            return reward;
        }

        public void setReward(int reward) {
            this.reward = reward;
        }

        public String getPayment_id() {
            return payment_id;
        }

        public void setPayment_id(String payment_id) {
            this.payment_id = payment_id;
        }

        public String getPayment_name() {
            return payment_name;
        }

        public void setPayment_name(String payment_name) {
            this.payment_name = payment_name;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getUser_player_id() {
            return user_player_id;
        }

        public void setUser_player_id(String user_player_id) {
            this.user_player_id = user_player_id;
        }

        public String getAdditional_information() {
            return additional_information;
        }

        public void setAdditional_information(String additional_information) {
            this.additional_information = additional_information;
        }

        public String getProduct_code() {
            return product_code;
        }

        public void setProduct_code(String product_code) {
            this.product_code = product_code;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public int getPayment_code() {
            return payment_code;
        }

        public void setPayment_code(int payment_code) {
            this.payment_code = payment_code;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public PaymentChildBean getPayment() {
            return payment;
        }

        public void setPayment(PaymentChildBean payment) {
            this.payment = payment;
        }

        public static class PaymentChildBean {
            private String account_number;

            public String getAccount_number() {
                return account_number;
            }

            public void setAccount_number(String account_number) {
                this.account_number = account_number;
            }
        }
    }

    public static class PaymentBean {
        /**
         * status_code : 201
         * status_message : GO-PAY transaction is created
         * transaction_id : 83ce2c45-6132-4b14-ad9d-d9765518e6b4
         * order_id : VCGAME-ORD-1582691083000
         * merchant_id : G184637003
         * gross_amount : 10200.00
         * currency : IDR
         * payment_type : gopay
         * transaction_time : 2020-02-26 23:24:44
         * transaction_status : pending
         * fraud_status : accept
         * actions : [{"name":"generate-qr-code","method":"GET","url":"https://api.sandbox.veritrans.co.id/v2/gopay/83ce2c45-6132-4b14-ad9d-d9765518e6b4/qr-code"},{"name":"deeplink-redirect","method":"GET","url":"https://simulator.sandbox.midtrans.com/gopay/ui/checkout?referenceid=p1c1xBuTRo&callback_url=http%3A%2F%2Fvcgamers.com%3Forder_id%3DVCGAME-ORD-1582691083000"},{"name":"get-status","method":"GET","url":"https://api.sandbox.veritrans.co.id/v2/83ce2c45-6132-4b14-ad9d-d9765518e6b4/status"},{"name":"cancel","method":"POST","url":"https://api.sandbox.veritrans.co.id/v2/83ce2c45-6132-4b14-ad9d-d9765518e6b4/cancel"}]
         */

        private String status_code;
        private String status_message;
        private String transaction_id;
        private String order_id;
        private String merchant_id;
        private String gross_amount;
        private String currency;
        private String payment_type;
        private String transaction_time;
        private String transaction_status;
        private String fraud_status;
        private List<ActionsBean> actions;
        private VANumbers va_numbers;

        public String getStatus_code() {
            return status_code;
        }

        public void setStatus_code(String status_code) {
            this.status_code = status_code;
        }

        public String getStatus_message() {
            return status_message;
        }

        public void setStatus_message(String status_message) {
            this.status_message = status_message;
        }

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getMerchant_id() {
            return merchant_id;
        }

        public void setMerchant_id(String merchant_id) {
            this.merchant_id = merchant_id;
        }

        public String getGross_amount() {
            return gross_amount;
        }

        public void setGross_amount(String gross_amount) {
            this.gross_amount = gross_amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public String getTransaction_time() {
            return transaction_time;
        }

        public void setTransaction_time(String transaction_time) {
            this.transaction_time = transaction_time;
        }

        public String getTransaction_status() {
            return transaction_status;
        }

        public void setTransaction_status(String transaction_status) {
            this.transaction_status = transaction_status;
        }

        public String getFraud_status() {
            return fraud_status;
        }

        public void setFraud_status(String fraud_status) {
            this.fraud_status = fraud_status;
        }

        public List<ActionsBean> getActions() {
            return actions;
        }

        public void setActions(List<ActionsBean> actions) {
            this.actions = actions;
        }

        public VANumbers getVa_numbers() {
            return va_numbers;
        }

        public void setVa_numbers(VANumbers va_numbers) {
            this.va_numbers = va_numbers;
        }

        public static class ActionsBean {
            /**
             * name : generate-qr-code
             * method : GET
             * url : https://api.sandbox.veritrans.co.id/v2/gopay/83ce2c45-6132-4b14-ad9d-d9765518e6b4/qr-code
             */

            private String name;
            private String method;
            private String url;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getMethod() {
                return method;
            }

            public void setMethod(String method) {
                this.method = method;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }

        public static class VANumbers {
            /**
             * name : generate-qr-code
             * method : GET
             * url : https://api.sandbox.veritrans.co.id/v2/gopay/83ce2c45-6132-4b14-ad9d-d9765518e6b4/qr-code
             */

            private String bank;
            private String va_number;

            public String getBank() {
                return bank;
            }

            public void setBank(String bank) {
                this.bank = bank;
            }

            public String getVa_number() {
                return va_number;
            }

            public void setVa_number(String va_number) {
                this.va_number = va_number;
            }
        }
    }
}
