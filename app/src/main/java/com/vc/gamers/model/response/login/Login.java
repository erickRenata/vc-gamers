package com.vc.gamers.model.response.login;

/**
 * Created by @erickrenata on 2020-01-07.
 */

public class Login {

    /**
     * token_type : Bearer
     * expires_in : 31622400
     * access_token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU2NmFhNTlhYTA4YWE3NmMxYmFkNWU0NzRiOWE0ZGIyYWZhYTZiYmM0MGNlMGI0OTNhMzFhMWI1NmNmYzc1NzA5OGNkNDA0NmYyNjVmM2FiIn0.eyJhdWQiOiI0IiwianRpIjoiZTY2YWE1OWFhMDhhYTc2YzFiYWQ1ZTQ3NGI5YTRkYjJhZmFhNmJiYzQwY2UwYjQ5M2EzMWExYjU2Y2ZjNzU3MDk4Y2Q0MDQ2ZjI2NWYzYWIiLCJpYXQiOjE1NzgzOTMyNjYsIm5iZiI6MTU3ODM5MzI2NiwiZXhwIjoxNjEwMDE1NjY2LCJzdWIiOiI1Iiwic2NvcGVzIjpbIioiXX0.nYVHcAFDbHFPfY4T8oJfaacN3AT_SpTFIoTjqd-FCpaKoq4J4mxFb-tFatqWWv8uaScb3uX9gim2LIQmDqWzOptJJFXFnOruubDFh6Sx_JCxyad2SmwyZkeeTDYvUwjT2via5bx97u0y7B03mFQZx6PhwG4HFL8aiSVNIEmBYoKJIAgY9c_xm5xCGD0CTLxUqU7vMYkdW34HQ92vmnWFQupVE_gG_x82MSmEB9NkzROpXHO24xGq9klHMNfYFTIh0TCH0lpCfo8_XgPDhzlMEcjR1KS6UGZD5xS7YEhtyKIdWkuB5M7FCu_Bn_amsl1YhBlmtL5O0ooMitNX9RSP9Q54HO2kwWc6Nx41Nv4eZw8sj5Jou4Nf3VOrbjQz5XrhRUnr8jXbLnq3V8qElPo7q1SBS7v5aJi4qm00INlYciACoeVLZYOBcWGYPkwE2HMGZXju4lwULfemOeazPdg2PXTSoSUNzCjifLelMXNPX3BsrkbGZhBebwp2bH1VFeFI6zI5EodwC6oaieVuucm1jxX56UumuN_ilLdpnkFLvZ1c5N6iFz-vYrzDlaxV99BjW0YQ02DMfQ1dVFbg5itIJmSaIWvpnJOATvvZqAlzLnaQM1gcvQgn3fwiwNS22gPfaC6GU41TWeVbDUNwDDByqRX1on6XqGGTdzZA5UJSuMw
     * refresh_token : def502009ef2e0f77d9e61b6122a79543bbe54048bfa46e42ea014abf2a4050857bca69b57f1541298ee70d609f0cf5e041fbd34cdee94b959deb0781fad8347ec6efa5ed42ff81f8bcf4514ac31678f3cfe8689f9096b2f653e9a371095d58619b0727153b25ffef572edc8e3f8ef9b9308e76f083b2a610e3024650a025febcede4338acc2f0dc9d9a1bec8d456e119477bc6754786ae616861b868bdefdfd3364bd31e03dd59775e580e3893273d8ed9028c5dacdf04025abf20cf6e8427f5099d705129fcbe8b228082648ed4015435eef38bf79dfcf9786d63cf197faae9e7ae7982a51017fe940021b956429be129bfa7713105ace82ddfb7ee9d2fd4eae799fe0b9072a2a9bad94206eedf760e025b79ec5862713ded3f86b16f7dc0b76be0e423962bba9f048f4a12775fd9920202824ce9afc80eb27eb65551941038e2339c24d899950497935af12f605f412461a25d63254b72a47108075b873ba1df3f3d8
     */

    private String token_type;
    private int expires_in;
    private String access_token;
    private String refresh_token;
    private String message;

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
