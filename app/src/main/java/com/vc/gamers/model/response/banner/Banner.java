package com.vc.gamers.model.response.banner;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by @erickrenata on 2020-01-20.
 */

public class Banner implements Parcelable {

    /**
     * id : 3
     * image : https://cdn-vcgamers.developerrizki.online/banner/6de77de95df9ac6fdbf8093447a591f5.png
     * url :
     * status : 1
     * user_id : 1
     * article_id : 5
     * article_type : promo
     * created_at : 2020-02-03 04:18:03
     * updated_at : 2020-02-03 04:18:03
     */

    private int id;
    private String image;
    private String url;
    private int status;
    private int user_id;
    private int article_id;
    private String article_type;
    private String created_at;
    private String updated_at;

    protected Banner(Parcel in) {
        id = in.readInt();
        image = in.readString();
        url = in.readString();
        status = in.readInt();
        user_id = in.readInt();
        article_id = in.readInt();
        article_type = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(image);
        dest.writeString(url);
        dest.writeInt(status);
        dest.writeInt(user_id);
        dest.writeInt(article_id);
        dest.writeString(article_type);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Banner> CREATOR = new Creator<Banner>() {
        @Override
        public Banner createFromParcel(Parcel in) {
            return new Banner(in);
        }

        @Override
        public Banner[] newArray(int size) {
            return new Banner[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public String getArticle_type() {
        return article_type;
    }

    public void setArticle_type(String article_type) {
        this.article_type = article_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
