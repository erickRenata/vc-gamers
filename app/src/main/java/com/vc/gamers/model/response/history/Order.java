package com.vc.gamers.model.response.history;

/**
 * Created by @erickrenata on 2020-01-19.
 */

public class Order {

    /**
     * id : 0
     * date : 2019-12-24 09:36:57
     * user_id : 4
     * denomination_id : 1
     * product_name : POKEMON GO
     * amount : 10000
     * amount_in_currency : IDR 100000
     * subtotal : 100000
     * fee : 0
     * grand_total : 100000
     * reward : 1
     * payment_id : 1
     * payment_name : Pembayaran Manual
     * payment_type : manual
     * status : 4
     * label : Menunggu Pembayaran
     * created_at : 2019-12-24 21:36:57
     * updated_at : 2019-12-24 21:36:57
     */

    private String  id;
    private String date;
    private String image;
    private int user_id;
    private int denomination_id;
    private String product_name;
    private int amount;
    private String amount_in_currency;
    private int subtotal;
    private int fee;
    private int grand_total;
    private int reward;
    private int payment_id;
    private String payment_name;
    private String payment_type;
    private int status;
    private String label;
    private String created_at;
    private String updated_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getDenomination_id() {
        return denomination_id;
    }

    public void setDenomination_id(int denomination_id) {
        this.denomination_id = denomination_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getAmount_in_currency() {
        return amount_in_currency;
    }

    public void setAmount_in_currency(String amount_in_currency) {
        this.amount_in_currency = amount_in_currency;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public int getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(int grand_total) {
        this.grand_total = grand_total;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public int getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(int payment_id) {
        this.payment_id = payment_id;
    }

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
