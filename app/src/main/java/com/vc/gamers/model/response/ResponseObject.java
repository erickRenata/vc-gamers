package com.vc.gamers.model.response;

import com.vc.gamers.base.BaseResponse;

/**
 * Created by @erickrenata on 2020-01-09.
 */

public class ResponseObject<T> extends BaseResponse {

    private T data;

    public T getData() {
        return data;
    }
}