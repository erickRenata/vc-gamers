package com.vc.gamers.network;

import android.util.Log;

import com.google.gson.GsonBuilder;
import com.vc.gamers.persistence.Preferences;

import java.lang.reflect.Modifier;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vc.gamers.constant.Constant.BASE_URL;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public class NetworkModule {

    public static Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .client(provideOkHttpClient())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                        .serializeNulls()
                        .create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static OkHttpClient provideOkHttpClient() {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        HttpLoggingInterceptor interceptor =
                new HttpLoggingInterceptor(message -> Log.d("VCGamers", message));
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okClient = new OkHttpClient.Builder().
                connectTimeout(40, TimeUnit.SECONDS).
                readTimeout(40, TimeUnit.SECONDS).
                cookieJar(new JavaNetCookieJar(cookieManager)).
                addInterceptor(interceptor).
//                addInterceptor(new ChuckInterceptor(VCGamers.getContext())).
        addInterceptor(chain -> {
    Request request = chain.request();

    String token = "";
    if (Preferences.getLoginResponse() != null) {
        token = Preferences.getLoginResponse().getAccess_token();
    }
    Request newRequest = request.newBuilder()
            .addHeader("Authorization", "Bearer " + token)
            .addHeader("Cache-Control", "no-cache")
            .addHeader("Cache-Control", "no-store")
            .build();

    return chain.proceed(newRequest);
}).build();

        return okClient;
    }
}
