package com.vc.gamers.network;

import com.vc.gamers.base.BaseResponse;
import com.vc.gamers.model.response.ResponseList;
import com.vc.gamers.model.response.ResponseObject;
import com.vc.gamers.model.response.article.Article;
import com.vc.gamers.model.response.banner.Banner;
import com.vc.gamers.model.response.checkout.Checkout;
import com.vc.gamers.model.response.game.DetailProduct;
import com.vc.gamers.model.response.game.Product;
import com.vc.gamers.model.response.history.Order;
import com.vc.gamers.model.response.login.Login;
import com.vc.gamers.model.response.payment.Payment;
import com.vc.gamers.model.response.product.Category;
import com.vc.gamers.model.response.product.Genre;
import com.vc.gamers.model.response.product.Type;
import com.vc.gamers.model.response.profile.Profile;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by @erickrenata on 2019-12-03.
 */

public interface ApiService {

    /***** Auth *****/
    // SIGN IN
    @FormUrlEncoded
    @POST("oauth/token")
    Observable<Login> postLogin(@Field("grant_type") String grantType,
                                @Field("client_id") String clientId,
                                @Field("client_secret") String clientSecret,
                                @Field("username") String username,
                                @Field("password") String password);

    // SIGN IN MEDSOS
    @FormUrlEncoded
    @POST("user/social_auth")
    Observable<Login> postLoginFacebook(@Field("email") String email,
                                        @Field("provider") String provider,
                                        @Field("authToken") String authToken,
                                        @Field("idToken") String idToken,
                                        @Field("photoUrl") String photoUrl,
                                        @Field("name") String name);

    // REGISTER
    @FormUrlEncoded
    @POST("register")
    Observable<Login> postRegister(@Field("email") String email,
                                          @Field("name") String name,
                                          @Field("password") String password,
                                          @Field("pin") String pin);
//    @Field("phone_number") String phone_number,
//    @Field("sex") String sex,
//    @Field("birth_date") String birth_date,

    // PROFILE
    @GET("profile")
    Observable<ResponseObject<Profile>> getProfile();

    // EDIT PROFILE
    @Multipart
    @POST("profile/update")
    Observable<BaseResponse> postUpdateProfile(@Part("name") RequestBody name,
                                               @Part("email") RequestBody email,
                                               @Part("phone_number") RequestBody phoneNumber,
                                               @Part("birth_date") RequestBody birth_date,
                                               @Part MultipartBody.Part avatar);
    @FormUrlEncoded
    @POST("profile/update")
    Observable<BaseResponse> postUpdateProfile(@Field("name") String name,
                                               @Field("email") String email,
                                               @Field("phone_number") String phone_number,
                                               @Field("birth_date") String birth_date);

    // UPDATE PASSWORD
    @FormUrlEncoded
    @POST("password/change")
    Observable<BaseResponse> postUpdatePassword(@Field("current_password") String currentPassword,
                                                @Field("password") String newPassword,
                                                @Field("password_confirmation") String passwordConfirmation);

    // FORGOT PASSWORD
    @FormUrlEncoded
    @POST("user/forgot_password")
    Observable<BaseResponse> postForgotPassword(@Field("email") String email);

    /***** Payment Method *****/
    // PAYMENT METHOD LIST
    @GET("payment")
    Observable<ResponseList<Payment>> getPaymentMethodList();

    // CHECKOUT
    @FormUrlEncoded
    @POST("order")
    Observable<ResponseObject<Checkout>> postCheckout(@Field("denomination_id") String denomination_id,
                                                      @Field("product_name") String product_name,
                                                      @Field("product_type") String product_type,
                                                      @Field("amount") String amount,
                                                      @Field("amount_in_currency") String amount_in_currency,
                                                      @Field("subtotal") String subtotal,
                                                      @Field("fee") String fee,
                                                      @Field("grand_total") String grand_total,
                                                      @Field("reward") String reward,
                                                      @Field("payment_id") String payment_id,
                                                      @Field("payment_name") String payment_name,
                                                      @Field("payment_type") String payment_type,
                                                      @Field("payment_method_category_id") String payment_method_category_id,
                                                      @Field("user_player_id") String user_player_id,
                                                      @Field("additional_information") String additional_information,
                                                      @Field("email") String email,
                                                      @Field("phone_number") String phone_number);

    /***** Article *****/
    // ARTICLE
    @GET("article/")
    Observable<ResponseList<Article>> getArticle();

    // ARTICLE DETAIL
    @GET("article/detail/{idArticle}")
    Observable<ResponseObject<Article>> getDetailArticle(@Path("idArticle") String idArticle);

    // NEWS
    @GET("article/news")
    Observable<ResponseList<Article>> getNewsList();

    // PROMO
    @GET("article/promo")
    Observable<ResponseList<Article>> getPromoList();

    /***** Product *****/
    // PRODUCT SEARCH
    @GET("product/search")
    Observable<ResponseList<Product>> getProductSearch(@Query("name") String name);

    // PRODUCT LIST
    @GET("product")
    Observable<ResponseList<Product>> getProductList(@Query("page") String page,
                                                     @Query("sort") String sort,
                                                     @Query("type") String type,
                                                     @Query("order") String order,
                                                     @Query("category_id") String categoryId,
                                                     @Query("genre_id") String genreId);

    // PRODUCT LIST ALL
    @GET("product")
    Observable<ResponseList<Product>> getProductListAll(@Query("page") String page,
                                                        @Query("sort") String sort,
                                                        @Query("order") String order);

    // PRODUCT TOP
    @GET("product/top")
    Observable<ResponseList<Product>> getProductTopList();

    // DETAIL PRODUCT
    @GET("product/detail/{idGame}")
    Observable<ResponseObject<DetailProduct>> getDetailProduct(@Path("idGame") String idGame);

    // PRODUCT CATEGORY
    @GET("product/category")
    Observable<ResponseList<Category>> getProductCategory();

    // PRODUCT GENRE
    @GET("product/genre")
    Observable<ResponseList<Genre>> getProductGenre();

    // PRODUCT TYPE
    @GET("product/type")
    Observable<ResponseList<Type>> getProductType();

    /***** History *****/
    // ORDER
    @GET("order/history")
    Observable<ResponseList<Order>> getPurchasedOrder();

    /***** Banner *****/
    // BANNER
    @GET("banner")
    Observable<ResponseList<Banner>> getBannerList();

}
